-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 08, 2019 at 08:33 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asceic_opss`
--

-- --------------------------------------------------------

--
-- Table structure for table `opss_co_author`
--

CREATE TABLE `opss_co_author` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `designation` varchar(150) NOT NULL,
  `organization` varchar(150) NOT NULL,
  `photo_url` varchar(255) DEFAULT NULL,
  `corresponding_author` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_co_author`
--

INSERT INTO `opss_co_author` (`id`, `paper_id`, `email`, `name`, `designation`, `organization`, `photo_url`, `corresponding_author`, `created_by`, `updated_by`, `created_on`, `updated_on`) VALUES
(25, 32, 'deyuddipan@gmail.com', 'Uddipan', 'Developer', 'Anonymous', '26165609_1394986637295123_1438802100356716655_n.jpg', 0, 30, 30, '2019-06-08 14:01:26', '2019-06-08 14:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `opss_file_type`
--

CREATE TABLE `opss_file_type` (
  `id` int(11) NOT NULL,
  `file_type` varchar(150) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_file_type`
--

INSERT INTO `opss_file_type` (`id`, `file_type`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'Cover Letter', '2019-06-07 09:00:45', '2019-06-07 09:00:45', 9, 9),
(2, 'Highlight', '2019-06-07 09:00:45', '2019-06-07 09:00:45', 9, 9),
(3, 'Graphical Abstruct', '2019-06-07 09:00:45', '2019-06-07 09:00:45', 9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `opss_form_step`
--

CREATE TABLE `opss_form_step` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `form_step` int(6) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_form_step`
--

INSERT INTO `opss_form_step` (`id`, `paper_id`, `form_step`, `updated_on`) VALUES
(9, 32, 4, '2019-06-08 17:48:22'),
(10, 33, 2, '2019-06-08 14:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `opss_permissions`
--

CREATE TABLE `opss_permissions` (
  `id` int(11) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `opss_research_paper`
--

CREATE TABLE `opss_research_paper` (
  `id` int(11) NOT NULL,
  `sub_theme_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `abstruct` text NOT NULL,
  `keywords` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_research_paper`
--

INSERT INTO `opss_research_paper` (`id`, `sub_theme_id`, `topic_id`, `title`, `abstruct`, `keywords`, `status`, `payment_status`, `created_by`, `updated_by`, `created_on`, `updated_on`) VALUES
(32, 2, 4, 'Test', 'Test', 'Test1,Test2', 'pending', 0, 30, 30, '2019-06-08 14:00:39', '2019-06-08 14:00:39'),
(33, 2, 4, 'Test 2', 'Test2', 'Test1', 'pending', 0, 30, 30, '2019-06-08 14:25:41', '2019-06-08 14:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `opss_reviewer`
--

CREATE TABLE `opss_reviewer` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `organization` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `photo_url` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_reviewer`
--

INSERT INTO `opss_reviewer` (`id`, `paper_id`, `email`, `name`, `designation`, `organization`, `country`, `note`, `photo_url`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(9, 32, 'deyuddipan@gmail.com', 'Uddipan', 'Developer', 'Anonymous', 'India', 'Test', '26165609_1394986637295123_1438802100356716655_n1.jpg', '2019-06-08 14:04:07', 30, '2019-06-08 14:04:07', 30);

-- --------------------------------------------------------

--
-- Table structure for table `opss_role`
--

CREATE TABLE `opss_role` (
  `id` int(11) NOT NULL,
  `role` varchar(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_role`
--

INSERT INTO `opss_role` (`id`, `role`, `created_by`, `created_on`, `updated_on`, `updated_by`) VALUES
(1, 'superadmin', 9, '2019-06-04 11:59:09', '2019-06-04 11:59:09', 9),
(2, 'admin', 9, '2019-06-04 11:59:13', '2019-06-04 11:59:13', 9),
(3, 'author', 9, '2019-06-04 11:59:17', '2019-06-04 11:59:17', 9),
(4, 'reviewer', 9, '2019-06-04 11:59:21', '2019-06-04 11:59:21', 9);

-- --------------------------------------------------------

--
-- Table structure for table `opss_role_permission_map`
--

CREATE TABLE `opss_role_permission_map` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `opss_sub_theme`
--

CREATE TABLE `opss_sub_theme` (
  `id` int(11) NOT NULL,
  `sub_theme` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_sub_theme`
--

INSERT INTO `opss_sub_theme` (`id`, `sub_theme`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'Sustainable infrastructure planning', '2019-06-05 13:34:39', '2019-06-05 13:34:39', 9, 9),
(2, 'Sustainable and resilient materials and innovative structural design ', '2019-06-05 13:34:47', '2019-06-05 13:34:47', 9, 9),
(3, 'Sustainable management of water & sanitation systems', '2019-06-05 13:34:53', '2019-06-05 13:34:53', 9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `opss_topic`
--

CREATE TABLE `opss_topic` (
  `id` int(11) NOT NULL,
  `sub_theme_id` int(11) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_topic`
--

INSERT INTO `opss_topic` (`id`, `sub_theme_id`, `topic`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 1, 'ICT for enhancing sustainability and resilience ', '2019-06-06 18:08:25', '2019-06-06 18:08:25', 9, 9),
(2, 2, 'Energy efficient planning for infrastructure development', '2019-06-06 18:08:29', '2019-06-06 18:08:29', 9, 9),
(3, 3, 'Safe and congestion free smart transport system ', '2019-06-06 18:08:33', '2019-06-06 18:08:33', 9, 9),
(4, 2, 'Environmental study', '2019-06-06 18:45:34', '2019-06-06 18:45:34', 9, 9);

-- --------------------------------------------------------

--
-- Table structure for table `opss_upload_files`
--

CREATE TABLE `opss_upload_files` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `sequence` int(10) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_upload_files`
--

INSERT INTO `opss_upload_files` (`id`, `paper_id`, `file_name`, `type`, `description`, `url`, `sequence`, `created_by`, `updated_by`, `created_on`, `updated_on`) VALUES
(28, 32, '2011.pdf', '1', 'Test1', '2011.pdf', 1, 30, 30, '2019-06-08 18:24:09', '2019-06-08 18:24:09'),
(29, 32, '2010.pdf', '1', 'Test 2', '2010.pdf', 0, 30, 30, '2019-06-08 18:24:09', '2019-06-08 18:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `opss_user`
--

CREATE TABLE `opss_user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_user`
--

INSERT INTO `opss_user` (`id`, `email`, `password`, `status`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(9, 'deyuddipan@gmail.com', '2f23f0d8d6d33f97ca5f921600481338', 1, '2019-06-03 15:37:50', '2019-06-03 15:37:50', 1, 1),
(29, 'sucharita.tg@gmail.com', '2f23f0d8d6d33f97ca5f921600481338', 0, '2019-06-05 10:15:42', '2019-06-05 10:15:42', 29, 29),
(30, 'reejudey@gmail.com', '2f23f0d8d6d33f97ca5f921600481338', 1, '2019-06-05 12:07:52', '2019-06-05 12:07:52', 30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `opss_user_details`
--

CREATE TABLE `opss_user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(70) NOT NULL,
  `organization` varchar(150) NOT NULL,
  `address_line_1` text NOT NULL,
  `address_line_2` text NOT NULL,
  `post_code` varchar(15) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `mobile` varchar(14) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_user_details`
--

INSERT INTO `opss_user_details` (`id`, `user_id`, `name`, `designation`, `organization`, `address_line_1`, `address_line_2`, `post_code`, `city`, `state`, `country`, `mobile`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 30, 'Uddipan Dey', 'Test', 'Test', 'Test', 'Test', '712222', 'Test', 'Test', 'India', '712222', '2019-06-05 12:08:48', '2019-06-05 12:08:48', 30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `opss_user_role_map`
--

CREATE TABLE `opss_user_role_map` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opss_user_role_map`
--

INSERT INTO `opss_user_role_map` (`id`, `user_id`, `role_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(48, 9, 1, '2019-06-03 14:42:29', '2019-06-03 14:42:29', 9, 9),
(50, 30, 3, '2019-06-05 12:09:04', '2019-06-05 12:09:04', 30, 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `opss_co_author`
--
ALTER TABLE `opss_co_author`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `opss_file_type`
--
ALTER TABLE `opss_file_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opss_form_step`
--
ALTER TABLE `opss_form_step`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opss_permissions`
--
ALTER TABLE `opss_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opss_research_paper`
--
ALTER TABLE `opss_research_paper`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_theme_id` (`sub_theme_id`),
  ADD KEY `topic_id` (`topic_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `opss_reviewer`
--
ALTER TABLE `opss_reviewer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paper_id` (`paper_id`);

--
-- Indexes for table `opss_role`
--
ALTER TABLE `opss_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opss_role_permission_map`
--
ALTER TABLE `opss_role_permission_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `opss_sub_theme`
--
ALTER TABLE `opss_sub_theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opss_topic`
--
ALTER TABLE `opss_topic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_theme_id` (`sub_theme_id`);

--
-- Indexes for table `opss_upload_files`
--
ALTER TABLE `opss_upload_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paper_id` (`paper_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `opss_user`
--
ALTER TABLE `opss_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opss_user_details`
--
ALTER TABLE `opss_user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `opss_user_role_map`
--
ALTER TABLE `opss_user_role_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `opss_user_role_map_ibfk_2` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `opss_co_author`
--
ALTER TABLE `opss_co_author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `opss_file_type`
--
ALTER TABLE `opss_file_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `opss_form_step`
--
ALTER TABLE `opss_form_step`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `opss_permissions`
--
ALTER TABLE `opss_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opss_research_paper`
--
ALTER TABLE `opss_research_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `opss_reviewer`
--
ALTER TABLE `opss_reviewer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `opss_role`
--
ALTER TABLE `opss_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `opss_role_permission_map`
--
ALTER TABLE `opss_role_permission_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opss_sub_theme`
--
ALTER TABLE `opss_sub_theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `opss_topic`
--
ALTER TABLE `opss_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `opss_upload_files`
--
ALTER TABLE `opss_upload_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `opss_user`
--
ALTER TABLE `opss_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `opss_user_details`
--
ALTER TABLE `opss_user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `opss_user_role_map`
--
ALTER TABLE `opss_user_role_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `opss_co_author`
--
ALTER TABLE `opss_co_author`
  ADD CONSTRAINT `opss_co_author_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `opss_research_paper` (`id`),
  ADD CONSTRAINT `opss_co_author_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `opss_user` (`id`);

--
-- Constraints for table `opss_research_paper`
--
ALTER TABLE `opss_research_paper`
  ADD CONSTRAINT `opss_research_paper_ibfk_1` FOREIGN KEY (`sub_theme_id`) REFERENCES `opss_sub_theme` (`id`),
  ADD CONSTRAINT `opss_research_paper_ibfk_2` FOREIGN KEY (`topic_id`) REFERENCES `opss_topic` (`id`),
  ADD CONSTRAINT `opss_research_paper_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `opss_user` (`id`);

--
-- Constraints for table `opss_reviewer`
--
ALTER TABLE `opss_reviewer`
  ADD CONSTRAINT `opss_reviewer_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `opss_research_paper` (`id`);

--
-- Constraints for table `opss_role_permission_map`
--
ALTER TABLE `opss_role_permission_map`
  ADD CONSTRAINT `opss_role_permission_map_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `opss_role` (`id`),
  ADD CONSTRAINT `opss_role_permission_map_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `opss_permissions` (`id`);

--
-- Constraints for table `opss_topic`
--
ALTER TABLE `opss_topic`
  ADD CONSTRAINT `opss_topic_ibfk_1` FOREIGN KEY (`sub_theme_id`) REFERENCES `opss_sub_theme` (`id`);

--
-- Constraints for table `opss_upload_files`
--
ALTER TABLE `opss_upload_files`
  ADD CONSTRAINT `opss_upload_files_ibfk_1` FOREIGN KEY (`paper_id`) REFERENCES `opss_research_paper` (`id`),
  ADD CONSTRAINT `opss_upload_files_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `opss_user` (`id`);

--
-- Constraints for table `opss_user_details`
--
ALTER TABLE `opss_user_details`
  ADD CONSTRAINT `opss_user_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `opss_user` (`id`);

--
-- Constraints for table `opss_user_role_map`
--
ALTER TABLE `opss_user_role_map`
  ADD CONSTRAINT `opss_user_role_map_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `opss_role` (`id`),
  ADD CONSTRAINT `opss_user_role_map_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `opss_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
