-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 03, 2019 at 05:42 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asceic_opms`
--

-- --------------------------------------------------------

--
-- Table structure for table `opms_city`
--

CREATE TABLE `opms_city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `state_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_city`
--

INSERT INTO `opms_city` (`id`, `name`, `state_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'Kolkata', 1, '2019-06-03 10:20:02', '2019-06-03 10:20:02', 1, 1),
(2, 'Bolepur', 1, '2019-06-03 10:20:02', '2019-06-03 10:20:02', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `opms_country`
--

CREATE TABLE `opms_country` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(4) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_country`
--

INSERT INTO `opms_country` (`id`, `name`, `code`, `created_on`, `updated_on`, `updated_by`, `created_by`) VALUES
(1, 'INDIA', 'IN', '2019-06-03 10:18:54', '2019-06-03 10:18:54', 1, 1),
(2, 'AUSTRALIA', 'AU', '2019-06-03 10:18:54', '2019-06-03 10:18:54', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `opms_permissions`
--

CREATE TABLE `opms_permissions` (
  `id` int(11) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `opms_role`
--

CREATE TABLE `opms_role` (
  `id` int(11) NOT NULL,
  `role` varchar(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_role`
--

INSERT INTO `opms_role` (`id`, `role`, `created_by`, `created_on`, `updated_on`, `updated_by`) VALUES
(1, 'superadmin', 1, '2019-06-03 10:15:10', '2019-06-03 10:15:10', 1),
(2, 'admin', 1, '2019-06-03 10:15:10', '2019-06-03 10:15:10', 1),
(3, 'author', 1, '2019-06-03 10:15:29', '2019-06-03 10:15:29', 1),
(4, 'reviewer', 1, '2019-06-03 10:15:29', '2019-06-03 10:15:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `opms_role_permission_map`
--

CREATE TABLE `opms_role_permission_map` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `opms_state`
--

CREATE TABLE `opms_state` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_state`
--

INSERT INTO `opms_state` (`id`, `name`, `country_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(1, 'West Bengal', 1, '2019-06-03 10:19:23', '2019-06-03 10:19:23', 1, 1),
(2, 'Uttarpradesh', 1, '2019-06-03 10:19:23', '2019-06-03 10:19:23', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `opms_user`
--

CREATE TABLE `opms_user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_user`
--

INSERT INTO `opms_user` (`id`, `email`, `password`, `status`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(9, 'deyuddipan@gmail.com', '2f23f0d8d6d33f97ca5f921600481338', 1, '2019-06-03 15:37:50', '2019-06-03 15:37:50', 1, 1),
(17, 'axt@gmail.com', '87db0285274fd123f3ec5fdf1ce486fa', 1, '2019-06-03 15:11:54', '2019-06-03 15:11:54', 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `opms_user_details`
--

CREATE TABLE `opms_user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(70) NOT NULL,
  `organization` varchar(150) NOT NULL,
  `address_line_1` text NOT NULL,
  `address_line_2` text NOT NULL,
  `post_code` varchar(15) NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `mobile` varchar(14) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_user_details`
--

INSERT INTO `opms_user_details` (`id`, `user_id`, `name`, `designation`, `organization`, `address_line_1`, `address_line_2`, `post_code`, `city_id`, `state_id`, `country_id`, `mobile`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(8, 17, 'Uddipan Dey', 'Manager', 'Test', 'Test', 'Test', '712222', 1, 1, 1, '4165465', '2019-06-03 15:13:15', '2019-06-03 15:13:15', 17, 17);

-- --------------------------------------------------------

--
-- Table structure for table `opms_user_role_map`
--

CREATE TABLE `opms_user_role_map` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opms_user_role_map`
--

INSERT INTO `opms_user_role_map` (`id`, `user_id`, `role_id`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES
(48, 9, 1, '2019-06-03 14:42:29', '2019-06-03 14:42:29', 9, 9),
(58, 17, 3, '2019-06-03 15:11:54', '2019-06-03 15:11:54', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `opms_city`
--
ALTER TABLE `opms_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`state_id`);

--
-- Indexes for table `opms_country`
--
ALTER TABLE `opms_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opms_permissions`
--
ALTER TABLE `opms_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opms_role`
--
ALTER TABLE `opms_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opms_role_permission_map`
--
ALTER TABLE `opms_role_permission_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `opms_state`
--
ALTER TABLE `opms_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `opms_user`
--
ALTER TABLE `opms_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opms_user_details`
--
ALTER TABLE `opms_user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `state_id` (`state_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `opms_user_role_map`
--
ALTER TABLE `opms_user_role_map`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `opms_city`
--
ALTER TABLE `opms_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `opms_country`
--
ALTER TABLE `opms_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `opms_permissions`
--
ALTER TABLE `opms_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opms_role`
--
ALTER TABLE `opms_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `opms_role_permission_map`
--
ALTER TABLE `opms_role_permission_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `opms_state`
--
ALTER TABLE `opms_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `opms_user`
--
ALTER TABLE `opms_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `opms_user_details`
--
ALTER TABLE `opms_user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `opms_user_role_map`
--
ALTER TABLE `opms_user_role_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `opms_city`
--
ALTER TABLE `opms_city`
  ADD CONSTRAINT `opms_city_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `opms_state` (`id`);

--
-- Constraints for table `opms_role_permission_map`
--
ALTER TABLE `opms_role_permission_map`
  ADD CONSTRAINT `opms_role_permission_map_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `opms_role` (`id`),
  ADD CONSTRAINT `opms_role_permission_map_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `opms_permissions` (`id`);

--
-- Constraints for table `opms_state`
--
ALTER TABLE `opms_state`
  ADD CONSTRAINT `opms_state_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `opms_country` (`id`);

--
-- Constraints for table `opms_user_details`
--
ALTER TABLE `opms_user_details`
  ADD CONSTRAINT `opms_user_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `opms_user` (`id`),
  ADD CONSTRAINT `opms_user_details_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `opms_city` (`id`),
  ADD CONSTRAINT `opms_user_details_ibfk_3` FOREIGN KEY (`state_id`) REFERENCES `opms_state` (`id`),
  ADD CONSTRAINT `opms_user_details_ibfk_4` FOREIGN KEY (`country_id`) REFERENCES `opms_country` (`id`);

--
-- Constraints for table `opms_user_role_map`
--
ALTER TABLE `opms_user_role_map`
  ADD CONSTRAINT `opms_user_role_map_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `opms_role` (`id`),
  ADD CONSTRAINT `opms_user_role_map_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `opms_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
