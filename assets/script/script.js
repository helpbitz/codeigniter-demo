particlesJS('particles-js',

  {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 5,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 6,
        "direction": "none",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true,
    "config_demo": {
      "hide_card": false,
      "background_color": "#b61924",
      "background_image": "",
      "background_position": "50% 50%",
      "background_repeat": "no-repeat",
      "background_size": "cover"
    }
  }

);


/*-- header fix--*/
function init() {
  window.addEventListener('scroll', function (e) {
    var distanceY = window.pageYOffset || document.documentElement.scrollTop,
      shrinkOn = 50,
      header = document.querySelector("header");
    if (distanceY > shrinkOn) {
      classie.add(header, "smaller");
    } else {
      if (classie.has(header, "smaller")) {
        classie.remove(header, "smaller");
      }
    }
  });
}
window.onload = init();

/*--sorting/rearrange--*/
$(function () {
  $(".sortable").sortable();
  $(".sortable").disableSelection();
});

$(document).ready(function () {
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
  /*--footable ---*/
  $('.tableformob').footable({
    "filtering": {
      "enabled": true
    }
  });

  /*-drag and drop-*/

  var droppedFiles = false;
  var fileName = '';
  var $dropzone = $('.dropzone');


  $dropzone.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
    e.preventDefault();
    e.stopPropagation();
  })
    .on('dragover dragenter', function () {
      $dropzone.addClass('is-dragover');
    })
    .on('dragleave dragend drop', function () {
      $dropzone.removeClass('is-dragover');
    })
    .on('drop', function (e) {
      droppedFiles = e.originalEvent.dataTransfer.files;
      fileName = droppedFiles[0]['name'];
      $('.dropname').html(fileName);
      $('.hideondrop').hide();
    });


  $("input:file").change(function () {
    fileName = $(this)[0].files[0].name;
    $('.dropname').html(fileName);
    $('.hideondrop').hide();
  });






  /*document ready end*/
});








