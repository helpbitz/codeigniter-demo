<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Model extends CI_Model{

	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
	}

	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';	
	public $rules = array();

	function get($id = NULL,$single = FALSE, $limit=NULL, $offset=NULL){
		if($id != NULL){
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key,$id);
			$method = 'row';
		}
		elseif($single == TRUE){
			$method = 'row';
		}
		else{
			$method = 'result';
		}

		if($limit !== NULL && $offset !== NULL){
			$this->db->limit($limit);
			$this->db->offset($offset);
		}
		
		try{
			$result = $this->db->get($this->_table_name);
			/* Check for any database error */
			$db_error = $this->db->error();
			if (!empty($db_error['code'])) {
				throw new Exception('Database error!');
				}
			else{
				if($result != false && $this->db->affected_rows()){
					return $result->$method();
				}
				else{
					return false;
				}
				
			}
		}
		catch (Exception $e) {
				
			return false;
		}
	}

		function get_by($where, $single = FALSE){
		try{
			$this->db->select('*')->from($this->_table_name)->where($where);
			/* Check for any database error */
			$db_error = $this->db->error();
			if (!empty($db_error['code'])) {
				throw new Exception('Database error!');
			}
			else{
				$result = $this->db->get();
				/* var_dump($result); exit; */
				if($result != false && $this->db->affected_rows() ){
					if($single)
						$result = $result->row();
					else
						$result = $result->result();

					return ['status'=>TRUE, 'result'=>$result, 'message'=>'successfull'];
				}
				else{
					return ['status'=>FALSE, 'message'=>'No record found'];
				}
			}
		}
		catch (Exception $e) {
				
			return ['status'=>FALSE, 'message'=>'Database error!'];
			
		}
		
	}
	
	/*  Save / Update data */
	function save($data, $id = NULL)
	{
		

		//insert
		if($id === NULL){
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;

			try {
			$this->db->set($data);
			$this->db->insert($this->_table_name);

			/* Check for any database error */
			$db_error = $this->db->error();
			if (!empty($db_error['code'])) {
				throw new Exception('Database error!');
				}
			else{
				$id = $this->db->insert_id(); /* If there is no error set current user id */
				return $id;
				}
			}
			catch (Exception $e) {
				
				return false;
			}
		}

		//update
		else{
			
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key,$id);
			$this->db->update($this->_table_name);
			return $this->db->affected_rows();
		}


		return $id;
	}

	/* Delete */
	function delete($id)
	{
		$filter = $this->_primary_filter;
		$id = $filter($id);

		if (!$id) {
			return FALSE;
		}

		$this->db->where($this->_primary_key,$id);
		$this->db->limit(1);
		$this->db->delete($this->_table_name);

		if ($this->db->error()['code']) {
			$result = 'Error! ['.$this->db->error()['message'].']';
			return ['status'=>false, 'message'=>$result];
		} else if (!$this->db->affected_rows()) {
			$result = 'Error! ID ['.$id.'] not found';
			return ['status'=>false, 'message'=>$result];
		} else {
			$result = 'Success';
			return ['status'=>true, 'message'=>$result];
		}
		
	}


	/* Get role id */
	function getRoleId($role){
		$this->db->select('id')->from('opss_role')->where(['role'=>$role]);
		return $this->db->get()->row();
	}



	/* Get role */
	function getRole($user_id){
		$result = $this->db->query('SELECT * FROM `opss_user_role_map` JOIN `opss_role` ON `role_id`=`opss_role`.`id` WHERE `user_id`='.$user_id);
		if($result->num_rows()){
			return $result->row();
		}
		else{
			return FALSE;
		}
	}
}
