<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PaperSubmissionModel extends MY_Model
{
    function __construct()
        {
            parent::__construct();
        }	
    protected $_table_name = 'opss_research_paper'; //Setting up table name
    protected $_primary_key = 'id'; //Setting up the primary key
    protected $_primary_filter = 'intval';

    /* Get Sub Theme start */
    public function getSubTheme(){
        $this->_table_name = 'opss_sub_theme';
        return $this->get();
    }
    /* Get Sub Theme end */

    /* Get Topics start*/
    public function getTopic($id=null){

        $this->_table_name = 'opss_topic';
        if($id)
        {
            $result = $this->get_by(['sub_theme_id'=>$id]);
            if($result['status'])
                return $result['result'];
            else    
                return [];
        }
        else
            return $this->get();
    }
    /* Get Topics end */

    /* Save Paper Information start */
    public function savePaperInfo($id = null){
        $this->_table_name = 'opss_research_paper';
        $data = array(
            'sub_theme_id'  =>  strip_tags($this->input->post('sub_theme')),
            'topic_id'      =>  strip_tags($this->input->post('topic')),
            'title'         =>  trim(strip_tags($this->input->post('title'))),
            'abstruct'      =>  trim(strip_tags($this->input->post('abstruct'))),
            'keywords'      =>  trim(strip_tags($this->input->post('keywords'))),
            'status'        =>  'pending',
            'created_on'    =>  date('Y-m-d H:i:s'),
            'created_by'    =>  $this->session->userdata('id'),
            'updated_by'    =>  $this->session->userdata('id')
        );
         /* Save or update data into database */
         $result = $this->save($data, $id);
         
         if($id == null){
            $this->_table_name = 'opss_form_step';
            $this->save(['paper_id'=>$result, 'form_step'=>2]);
         }
         
         return $result;
    }
    /* Save Paper Information end */

    /* Get Paper Info start */
    public function getPaperInfo($id){
        $this->_table_name = 'opss_research_paper';
        $data = $this->get($id, true);
        return $data;
    }
    /* Get Paper Info end */


    //Upload file
    public function upload_files($i){
       
        $_FILES['file']['name']     = $_FILES['files']['name'][$i];
        $_FILES['file']['type']     = $_FILES['files']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
        $_FILES['file']['error']     = $_FILES['files']['error'][$i];
        $_FILES['file']['size']     = $_FILES['files']['size'][$i];
        // File upload configuration
        $uploadPath = realpath(APPPATH . '../uploads');
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        
        // Load and initialize upload library
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        // Upload file to server
        
        if($this->upload->do_upload('file')){
            // Uploaded file data
            $data = array('upload_data' => $this->upload->data());
            return $data["upload_data"]["file_name"];
        }
          
    }
    /* Save Co Author Information start */
    public function saveCoAuthorInfo($id,$data){
        $this->_table_name = 'opss_co_author';
        if(!empty($data)){
        foreach($data as $key=>$value){
            $path = 'profile.png';
            if(!empty($_FILES['files']['name'][$key])){
                $path = $this->upload_files($key);
            }
            $dataArr = [
                'paper_id'      => $id,
                'email'         => $value->email,
                'name'          => $value->name,
                'designation'   => $value->desig,
                'organization'  => $value->org,
                'photo_url'     => $path,
                'corresponding_author' => $value->isCoAuthor,
                'created_on'    => date('Y-m-d H:i:s'),
                'created_by'    => $this->session->userdata('id'),
                'updated_by'    => $this->session->userdata('id')
            ];
            $result = $this->save($dataArr);
        } 
        $this->db->query('UPDATE `opss_form_step` SET `form_step` = 3 WHERE `paper_id`='.$id);
        return $result;
     }
     else 
        return false;
               
    }
    /* Save Co Author Information end */

    /* Get Co Author info start */
    public function getCoAuthorInfo($id){
        $this->_table_name = 'opss_co_author';
        $data = $this->get_by(['paper_id'=>$id]);
        if($data['status'])
            return $data['result'];
        else    
            return [];

    }
    /* Get Co Author info end */

    /* Get Reviewer info start */
    public function getReviewerInfo($id){
        $this->_table_name = 'opss_reviewer';
        $data = $this->get_by(['paper_id'=>$id]);
        if($data['status'])
            return $data['result'];
        else    
            return [];
    }
    /* Get Reviewer info end */

    /* Save Reviewer Information start */
    public function saveReviewerInfo($id,$data){
        $this->_table_name = 'opss_reviewer';
        if(!empty($data)){
            foreach($data as $key=>$value){
                $path = 'profile.png';
                if(!empty($_FILES['files']['name'][$key])){
                    $path = $this->upload_files($key);
                }
                $dataArr = [
                    'paper_id'      => $id,
                    'email'         => $value->email,
                    'name'          => $value->name,
                    'designation'   => $value->desig,
                    'organization'  => $value->org,
                    'country'       => $value->country,
                    'note'          => $value->note,
                    'photo_url'     => $path,
                    'created_on'    => date('Y-m-d H:i:s'),
                    'created_by'    => $this->session->userdata('id'),
                    'updated_by'    => $this->session->userdata('id')
                ];
                $result = $this->save($dataArr);
            } 
            $this->db->query('UPDATE `opss_form_step` SET `form_step` = 5 WHERE `paper_id`='.$id);
            return $result;
        }
        return false;
               
    }
    /* Save Reviewer Information end */

    /* Get current form step */
    public function getStep($id){
        $this->_table_name = 'opss_form_step';
        $result = $this->get_by(['paper_id'=>$id], true);
        if($result['status'])
            return $result['result']->form_step;
        else    
            return false;
    }

    /* File upload */
    public function FileUpload($id){
        $image_path = realpath(APPPATH . '../uploads');
            
        $config = array(
        'upload_path' => $image_path,
        'allowed_types' => "gif|jpg|png|jpeg|pdf",
        'overwrite' => TRUE,
        'max_size' => "7048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload('file') === TRUE)
        {
            $data = array('upload_data' => $this->upload->data());
            $file_name = $data["upload_data"]["file_name"];
            $this->_table_name = 'opss_upload_files';
            $data = [
                'paper_id' => $id,
                'file_name'=> $file_name,
                'type'     => $this->input->post('file_type'),
                'description' => $this->input->post('description'),
                'url'        => $file_name,
                'sequence'   => 1000,
                'created_on'    => date('Y-m-d H:i:s'),
                'created_by'    => $this->session->userdata('id'),
                'updated_by'    => $this->session->userdata('id')
            ];
            $this->db->query('UPDATE `opss_form_step` SET `form_step` = 4 WHERE `paper_id`='.$id);
            return $this->save($data);
        }
        return false;
    }

    /* Get file information */
    public function getFileInfo($id){
       
        return $query = $this->db->query('SELECT `uf`.id, `uf`.`file_name`, `ft`.`file_type`,`uf`.`description`,`uf`.`sequence` FROM `opss_upload_files` AS `uf` JOIN `opss_file_type` AS `ft` ON `uf`.`type` = `ft`.`id` WHERE `paper_id`='.$id.' ORDER BY `sequence`')->result();
    }

    /* Get file types */
    public function getFileTypes(){
        $this->_table_name = 'opss_file_type';
        return $this->get();
    }

    /* Reorder Files */
    public function reOrderFiles($data){
        $this->_table_name = 'opss_upload_files';
        foreach($data as $d){
            $this->save(['sequence'=>$d->sequence],$d->id);
        }
    }

    /* Merge Pdf */
    public function mergePdf($id, $allData){
        $data['allData'] = $allData;
        $mpdf = new Mpdf\Mpdf;
        $mpdf->WriteHTML( $this->load->view('paperSubmission/pdf-view', $data,true));

        // Output a PDF file directly to the browser
        $mpdf->Output(realpath(APPPATH . '../uploads').'/new.pdf','F');    
        // include realpath(APPPATH.'libraries/PDFMerger/PDFMerger.php');
        $pdf = new \Jurosh\PDFMerge\PDFMerger;
        $this->_table_name = 'opss_upload_files';
        $status = $this->get_by(['paper_id'=>$id]);
        if($status['status'])
            $data = $status['result'];
        $pdf->addPDF(realpath(APPPATH . '../uploads').'/new.pdf','all');
        foreach($data as $d){
            
            $url = realpath(APPPATH . '../uploads').'/'.$d->file_name;
            $pdf->addPDF($url, 'all');
        }
        $pdf_name = realpath(APPPATH . '../uploads').'/review.pdf';
        $pdf->merge('browser', $pdf_name);
    
    }

    /* Change Project status */
    public function changeStatus($id){
        $this->_table_name = 'opss_research_paper';
        $status = $this->save(['status'=>'submitted'],$id);
        return $status;
    }
    
    //Get full data for a paper
    public function getFullData($id){
        $data = array();
        //Get paper info
        $paper_info = $this->db->query('SELECT `title`,`abstruct`,`keywords`,`topic`,`sub_theme` FROM `opss_research_paper` AS `rp` JOIN `opss_topic` AS `t` ON `rp`.`topic_id`=`t`.`id` JOIN `opss_sub_theme` AS `st` ON `rp`.`sub_theme_id`=`st`.`id` WHERE `rp`.id='.$id)->row();
        if(!empty($paper_info))
            $data['paper_info'] = $paper_info;
        //Get co author info
        $co_author = $this->getCoAuthorInfo($id);
        if(!empty($co_author))
            $data['co_author'] = $co_author;
        return $data;
    }

    //Delete file
    public function deleteFile($fileId){
        $this->_table_name = 'opss_upload_files';
        $file_name = $this->get($fileId,true)->file_name;
        $file = realpath(APPPATH . '../uploads').'/'.$file_name;
        if(unlink($file))
            $this->delete($fileId);
        
    }
}