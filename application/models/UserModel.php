<?php defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends MY_Model
{
    function __construct()
        {
            parent::__construct();
        }	
    protected $_table_name = 'opss_user'; //Setting up table name
    protected $_primary_key = 'id'; //Setting up the primary key
    protected $_primary_filter = 'intval';

    /* Register Author start */
        public function register($data = array()){ 
            $time_created = date('Y-m-d H:i:s');
            $data['status'] = 0;
            $data['created_on'] = $time_created;
            /* Save data into database */
            $id = $this->save($data); 
            if($id !== FALSE){
                $update = array('created_by'=>$id, 'updated_by'=>$id);
                $result = $this->save($update,$id);
                $this->sendMail($data['email'], $id);
                return $result;
            }
            else {
                return FALSE;
            }
        }
    /* Register Author end */

    /* Send Verification mail start */
        public function sendMail($email, $id){
            $this->load->library('email');
            $config=array(
                'charset'=>'utf-8',
                'wordwrap'=> TRUE,
                'mailtype' => 'html'
            );
                
            $this->email->initialize($config);
            $this->email->from('no-reply@example.com', 'OPSS');
            $this->email->to($email);
            $this->email->subject('Email Test');
            $array = array();
            $array['id'] = $id;
            $array['email'] = $email;
            $str = serialize($array);
            $url = base_url().'index.php/users/registration-2/'.urlencode($str);
            $this->email->message(
                '<html>
                    <head>
                        <style>
                            @import url("https://fonts.googleapis.com/css?family=Poppins");
                            @import url("https://fonts.googleapis.com/css?family=Bangers");
                        </style> 
                    </head>
                    <body>
                        <p style="color:#3773a4;font-size: 14px; text-align: center;font-family: \'Bangers\';">Complete your registration by clicking the following link <br> “Grace Coddington” </p>
                        <div style="border-radius: 2px; text-align: center;">
                        <a href="'.$url.'" style="padding: 8px 12px; font-family: \'Poppins\', sans-serif;font-size: 12px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;background:#91d047; width: 50%">Complete Registration Process</a>
                        </div>
                    </body>
                </html>');
            if($this->email->send()){
                return TRUE;
            }else{
                return FALSE;
            }
        }
    /* Send Verification mail end */

    /* Activate Account start */
        public function activateAccount($id){
            /* Set status to 1 in opms_user */
            $this->_table_name = 'opss_user';
            $update = ['status' => 1, 'updated_by'=>1];
            $this->save($update,$id);

            /* get role id for author */
            $author_role_id = $this->getRoleId('author')->id;
            /* Map User id with author id */
            $this->_table_name = 'opss_user_role_map';
            $result = $this->get_by(['user_id'=>$id]);
            
            if(!$result['status']){
                $data = ['user_id'=>$id,'role_id'=>$author_role_id, 'created_on'=>date('Y-m-d H:i:s'), 'created_by'=>1, 'updated_by'=>1];
                $status = $this->save($data);
                return $status;
            }
        }
    /* Activate Account end */

    /* Fill user data start */
        public function fillUserData($id, $email){
            $this->activateAccount($id);
            $data = array(
                'user_id'       =>  $id,
                'name'          =>  strip_tags($this->input->post('first-name')) . " " . strip_tags                                   ($this->input->post('last-name')),
                'designation'   =>  strip_tags($this->input->post('designation')),
                'organization'  =>  strip_tags($this->input->post('organization')),
                'address_line_1'=>  strip_tags($this->input->post('address-line-1')),
                'address_line_2'=>  strip_tags($this->input->post('address-line-2')),
                'post_code'     =>  strip_tags($this->input->post('postcode')),
                'mobile'        =>  strip_tags($this->input->post('phone')),
                'city'          =>  $this->input->post('city'),
                'state'         =>  $this->input->post('state'),
                'country'       =>  $this->input->post('country'),
                'created_on'    =>  date('Y-m-d H:i:s'),
                'created_by'    =>  $id,
                'updated_by'    =>  $id
            );
             $this->_table_name = 'opss_user_details';
             /* Save data into database */
             $result = $this->save($data);
             $role = $this->getRole($id);
             //Get name
             $query = $this->db->query('SELECT `name` FROM `opss_user_details` WHERE `user_id`='.$id)->row();
             $name = $query->name;
             $session_data = ['id' => $id, 'email'=> $email, 'name'=> $name,  'role' => $role->role, 'is_loggedin'=>TRUE];
             $this->session->set_userdata($session_data); //Setting up the valid user details in the session

             return $result;
        }
    /* Fill user data end */

    /* User Login start */
        public function login(){
    
                /* Check for both valid email and password */
                $user = $this->get_by(['email'=>strip_tags($this->input->post('email')),
                                       'password' => md5($this->input->post('password'))], TRUE);
                if($user['status']){
                    //Check for user is blocked or not
                    if($user['result']->status){
                        //Role based login
                        $role = $this->getRole($user['result']->id);
                        if($role){
                            /* if($role->role == 'author'){ */
                                //Get name
                                $query = $this->db->query('SELECT `name` FROM `opss_user_details` WHERE `user_id`='.$user['result']->id)->row();
                                $name = $query->name;

                                $session_data = ['id' => $user['result']->id, 'name' => $name, 'email'=>$user['result']->email,  'role' => $role->role, 'is_loggedin'=>TRUE];
                                $this->session->set_userdata($session_data); //Setting up the valid user details in the session
                                return ['status'=> TRUE,'msg'=>"Successfully logged in"];

                            /* } */
                        }
                    }
                    else{
                        return ['status'=> FALSE,'msg'=>"Your account is not activated!"];
                    }  
                }
                else{
                   
                    return ['status'=> FALSE,'msg'=>"Invalid Email or Password"];
                }
            }
    /* User Login end */

    /* Check for already loggedin start */
        function loggedin(){
            return(bool)$this->session->userdata('is_loggedin');
        }
    /* Check for already loggedin end */

    /* User logout start */
        function logout(){
            $this->session->sess_destroy();
        }
    /* User logout end */

    /* Check for account */
    public function checkAccount($email){
         $status = $this->get_by(['email'=>$email]);
         if($status['status'])
            return TRUE;
         else
            return FALSE;
    }


}