<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends MY_Model
{
    function __construct()
    {
        parent::__construct();
    }	
    protected $_table_name = 'opss_research_paper'; //Setting up table name
    protected $_primary_key = 'id'; //Setting up the primary key
    protected $_primary_filter = 'intval';

    /* Get Papers */
    public function getPapers($id){
        
        $data = $this->db->query('SELECT `rp`.`id`,`title`,`abstruct`,`status`,`payment_status`,`rp`.`created_on`,`rp`.`updated_on`,`rp`.`created_by`,`t`.`topic` FROM `opss_research_paper` AS `rp` JOIN `opss_topic` AS `t` ON `rp`.`topic_id` = `t`.`id` WHERE `rp`.`created_by` ='.$id)->result();
        return $data;
    }

}