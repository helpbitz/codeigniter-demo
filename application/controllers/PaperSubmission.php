<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaperSubmission extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('PaperSubmissionModel');
        $this->load->model('UserModel');
        $this->data = [
            'email' =>  $this->session->userdata('email'), 
            'role'  =>  $this->session->userdata('role'), 
            'name'  =>  $this->session->userdata('name')
        ];
    }

    /* 
    * Redirect to step
     */
    public function redirect_to_step(){
        $id = $this->uri->segment('3');
        $step = $this->PaperSubmissionModel->getStep($id);
        if($step){
            if($step == 2)
                redirect('paper-submission/add-co-author/'.$id);
            else if($step == 3)
                redirect('paper-submission/upload-file/'.$id);
            else if($step == 4)
                redirect('paper-submission/add-reviewer/'.$id);
            else if($step == 5)
                redirect('paper-submission/review-submit/'.$id);
        }
        else{
            redirect('paper-submission/paper-information/'.$id);
        }
    }
   /*
   * New paper submission step 1
   */
   public function paper_information(){
  
    //redirect dashboard if not loggedin
    $this->UserModel->loggedin() == TRUE || redirect('users/login');
    $id = $this->uri->segment('3');
    $this->data['paper_info'] = [];
    //Get Subtheme
    $sub_themes = $this->PaperSubmissionModel->getSubTheme();
    $sub_themes !== FALSE ? $this->data['sub_themes'] = $sub_themes : $this->data['sub_themes'] = [];

    //Get Topic
    $topics = $this->PaperSubmissionModel->getTopic();
    $topics !== FALSE ? $this->data['topics'] = $topics : $this->data['topics'] = [];

    //Get Step
    $step = $this->PaperSubmissionModel->getStep($id);
    $this->data['step'] = $step;
    //Get paper information if id exists
    if($id !== NULL){
        $result = $this->PaperSubmissionModel->getPaperInfo($id);
        if(!empty($result))
            $this->data['paper_info'] = $result;     
            if($this->input->post('SaveBtn') || $this->input->post('SaveBtn2')){
                $status = $this->PaperSubmissionModel->savePaperInfo($id);
                if($this->input->post('SaveBtn2'))
                    redirect('paper-submission/add-co-author/'.$id);
                //Updated result
                $result = $this->PaperSubmissionModel->getPaperInfo($id);
                $this->data['paper_info'] = $result; 
                $this->data['paper_id'] = $id;

                if($status)
                    $this->data['msg'] = "Paper Information Updated Successfully";
                else    
                    $this->data['msg'] = "Some Error Occured..Please Try Again Later!";
            }     
        
    }
    else{ //For new submissions
       
        if($this->input->post('SaveBtn') || $this->input->post('SaveBtn2')){
            $status = $this->PaperSubmissionModel->savePaperInfo();
            if($status)
            {
                $this->data['msg'] = "Paper Information Saved Successfully";
                $this->data['paper_id'] = $status;
                if($this->input->post('SaveBtn2'))
                    redirect('paper-submission/add-co-author/'.$status);

            }
            else    
                $this->data['msg'] = "Some Error Occured..Please Try Again Later!";
        }
    }
        $this->load->view('paperSubmission/paper-information', $this->data);
   }

  /*
   * New paper submission step 2
   */
  public function add_co_author(){
    //redirect dashboard if not loggedin
    $this->UserModel->loggedin() == TRUE || redirect('users/login');
    $id = $this->uri->segment('3');
    $this->data['paper_id'] = $id;
    $result = $this->PaperSubmissionModel->getCoAuthorInfo($id);
    $step = $this->PaperSubmissionModel->getStep($id);
    if(!empty($result))
        $this->data['co_authors'] = $result;
    else
    {
        if($step < 2){
            redirect('paper-submission/redirect-to-step/'.$id);
        }
        
        $this->data['co_authors'] = [];
    }
    $this->data['step'] = $step;

    if($this->input->post('SaveBtn') || $this->input->post('SaveBtn2')){ 
       
        $data = json_decode($this->input->post('co_author_data'));
        $status = $this->PaperSubmissionModel->saveCoAuthorInfo($id,$data);
        if($status || !empty($result)){
            $result = $this->PaperSubmissionModel->getCoAuthorInfo($id);
            $this->data['co_authors'] = $result;
            if($this->input->post('SaveBtn2'))
                redirect('paper-submission/upload-file/'.$id);
    
        }
        
    }
    $this->load->view('paperSubmission/add-co-author', $this->data);

  }

   /*
   * New paper submission step 3
   */
  public function upload_files(){
    //redirect dashboard if not loggedin
    $this->UserModel->loggedin() == TRUE || redirect('users/login');
    $id = $this->uri->segment('3');
    if(!$id){
        redirect(base_url());
    }
    $this->data['paper_id'] = $id;

    //Get file types
    $data = $this->PaperSubmissionModel->getFileTypes();
    if($data)
        $this->data['file_types'] = $data;
    else
    {
       
        $this->data['file_types'] = [];
    }
       
    //upload files
    if($this->input->post('uploadFile')){
        if(!empty($_FILES['file'])){
            //Updated file info after upload
            $status = $this->PaperSubmissionModel->FileUpload($id);
            if($status){
               
            }
        }
    }
    //sort files
    if(($this->input->post('sortFileBtn') || $this->input->post('sortFileBtn2'))){
        $data = json_decode($this->input->post('sortFileOrders'));
        $this->PaperSubmissionModel->reOrderFiles($data);
        if($this->input->post('sortFileBtn2'))
            redirect('paper-submission/add-reviewer/'.$id);
    }
    //Delete file
    if($this->input->post('deleteFile')){
        $fileId = $this->input->post('deleteFile');
        $this->PaperSubmissionModel->deleteFile($fileId);
    }
    //File info if exits
    $data = $this->PaperSubmissionModel->getFileInfo($id);
    $step = $this->PaperSubmissionModel->getStep($id);
    if(!empty($data))
        $this->data['files'] = $data;
    else{
        
        if($step < 3){
            redirect('paper-submission/redirect-to-step/'.$id);
        }
        $this->data['files'] = [];

    }   
    $this->data['step'] = $step;   
    $this->load->view('paperSubmission/upload-file', $this->data);

  }


  /*
   * New paper submission step 4
   */
  public function add_reviewer(){
    //redirect dashboard if not loggedin
    $this->UserModel->loggedin() == TRUE || redirect('users/login');
    $id = $this->uri->segment('3');
    $this->data['paper_id'] = $id;
    $result = $this->PaperSubmissionModel->getReviewerInfo($id);
    $step = $this->PaperSubmissionModel->getStep($id);
    if(!empty($result))
        $this->data['reviewers'] = $result;
    else
    {
        
        if($step < 4){
            redirect('paper-submission/redirect-to-step/'.$id);
        }
        $this->data['reviewers'] = [];
    }
    $this->data['step'] = $step; 

    if($this->input->post('SaveBtn') || $this->input->post('SaveBtn2')){ 
        $data = json_decode($this->input->post('reviewer_data'));
        $status = $this->PaperSubmissionModel->saveReviewerInfo($id,$data);
        $result = $this->PaperSubmissionModel->getReviewerInfo($id);
        if($status || !empty($result)){
            
            $this->data['reviewers'] = $result;
            if($this->input->post('SaveBtn2'))
                redirect('paper-submission/review-submit/'.$id);
        }
    }
    $this->load->view('paperSubmission/add-reviewer', $this->data);

  }

  /*
   * New paper submission step 5
   */
  public function review_submit(){
    //redirect dashboard if not loggedin
    $this->UserModel->loggedin() == TRUE || redirect('users/login');
    $id = $this->uri->segment('3');
    if(!$id){
        redirect(base_url());
    }
    $this->data['paper_id'] = $id;
    if($this->input->post('downloadPdf')){
        $allData = $this->PaperSubmissionModel->getFullData($id);
        $this->PaperSubmissionModel->mergePdf($id, $allData);
    }
    if($this->input->post('completeSubmission')){
        if($this->PaperSubmissionModel->changeStatus($id))
            redirect(base_url());
    }
    //File info if exits
    $data = $this->PaperSubmissionModel->getFileInfo($id);
    if(!empty($data))
        $this->data['files'] = $data;
    else {
        $step = $this->PaperSubmissionModel->getStep($id);
        if($step < 5){
            redirect('paper-submission/redirect-to-step/'.$id);
        }
        $this->data['files'] = [];
    }   
        
    $this->load->view('paperSubmission/review-submit', $this->data);

  }

 
  //Function for dynamic selection of topic based on sub_theme
  public function getTopic(){
    
    $id = $this->uri->segment('3');
    $result = $this->PaperSubmissionModel->getTopic($id);
    print_r(json_encode($result));
  }

}