<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('UserModel');
    }
    /* 
     * Password validation 
     */
    public function password_check($str)
    {
        $uppercase = preg_match('@[A-Z]@', $str);
        $lowercase = preg_match('@[a-z]@', $str);
        $number    = preg_match('@[0-9]@', $str);
        $spcl_char = preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $str);
        
        if(!$uppercase || !$lowercase || !$number || !$spcl_char || strlen($str) < 8) {
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
	/*
     * User registration
     */
    public function register(){
        //redirect dashboard if loggedin
        $this->UserModel->loggedin() == FALSE || redirect('dashboard');

        $data = array();
        $userData = array();
        if($this->input->post('registerBtn')){
            /* Importing rules array */
            require APPPATH . 'validations/signUpValidationRules.php'; 
            $this->form_validation->set_rules($rules);
            $userData = array(
                'email' => strip_tags($this->input->post('email')),
                'password' => md5($this->input->post('password'))
            );
            if($this->form_validation->run() == true){
                $status = $this->UserModel->register($userData);
                if(!$status){
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
                else{
                    $data['success_msg'] = 'Registration successfull..Check your Email account for further verification.';
                }
            }
            
        }
        $data['user'] = $userData;
        //load the view
        $this->load->view('users/register', $data);
    }

    /* 
     *Paper submit form  
     */
    public function paper_submit_form(){
        //redirect dashboard if loggedin
        $this->UserModel->loggedin() == FALSE || redirect('dashboard');

        $str = $this->uri->segment('3');
        $arr = unserialize(urldecode($str));        
        $email = $arr['email'];
        $id = $arr['id'];
        $data = array();
        $data['email'] = $email;
        if($this->input->post('paperSubmitBtn')){
            /* Importing rules array */
            require APPPATH . 'validations/paperSubmitFormValidation.php'; 
            $this->form_validation->set_rules($rules);
        
        if($this->form_validation->run() == true){
            $status = $this->UserModel->fillUserData($id, $email);
            if(!$status){
                $data['error_msg'] = 'Some problems occured, please try again.';
            }
            else{
                redirect('dashboard');
            }
        }

        }
        $this->load->view('users/submit-paper', $data);
        
    }

    /* 
     *User Login
     */
    public function login(){
        //redirect dashboard if loggedin
        $this->UserModel->loggedin() == FALSE || redirect('dashboard');
        $data = array();
        $userData = array();
        if($this->input->post('loginBtn')){
            /* Importing rules array */
            require APPPATH . 'validations/logInValidationRules.php'; 
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() == true){
                $status = $this->UserModel->login();
                if(!$status['status']){
                    $data['error_msg'] = $status['msg'];
                }
                else{
                   
                    redirect('dashboard');
                }
            }
        }
        $data['user'] = ['email'=>strip_tags($this->input->post('email'))];
        $this->load->view('users/login', $data);
    }

    /* 
     *User Logout
     */
    public function logout(){
        $this->UserModel->logout();
        redirect('users/login');
    }

    /* 
     *Account Activation
     */
    public function activate_account(){
        $str = $this->uri->segment('3');
        $arr = unserialize(urldecode($str));        
        $email = $arr['email'];
        $status = $this->UserModel->checkAccount($email);
        if($status){
            $data['str'] = $str;
            $this->load->view('users/registration-2', $data);
        }
        else{
            redirect('users/login');
        }
        
    }
    
}
