<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('DashboardModel');
        $this->data = [
            'id'    =>  $this->session->userdata('id'),
            'email' =>  $this->session->userdata('email'), 
            'role'  =>  $this->session->userdata('role'), 
            'name'  =>  $this->session->userdata('name')
        ];
    }

    function index(){
        
        //redirect dashboard if not loggedin
        $this->UserModel->loggedin() == TRUE || redirect('users/login');
        $papers = $this->DashboardModel->getPapers($this->data['id']);
        $this->data['papers'] = $papers;
        $this->load->view('dashboard/dashboard',$this->data);
    }
}