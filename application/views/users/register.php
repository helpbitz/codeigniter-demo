<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASCE India conference 2020</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets');?>/images/favcon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('assets');?>/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/setup.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/popup-page.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="particles-js"></div>
    <a class="btn btn-secondary popBack" href="#" onclick="history.back(-1)">Close</a>
    <div class="prelogin">
        <div class="container-fluid" style="padding:0;">
            <div class="row">
                <div class="col-lg-4">
                    <div class="parent-div" style="height:100vh; padding-bottom:50px;">
                        <div class="div-child">
                            <div class="loginBx">
                                <img src="<?php echo base_url('assets');?>/images/logo.png" />
                                <div class="text-left mb-4 pb-2">
                                    <div class="mb-2 mt-4 font-bold text-2xl">
                                        Get started with your account
                                    </div>
                                    <div>Find your topics. Engage your audience. Build your reputation. Do it all with
                                        ASCEIC2020. </div>
                                </div>
                                <div class="text-danger">
                                    <?php
                                        if(!empty($success_msg)){
                                            echo '<p class="statusMsg">'.$success_msg.'</p>';
                                        }elseif(!empty($error_msg)){
                                            echo '<p class="statusMsg">'.$error_msg.'</p>';
                                        }
                                    ?>
                                </div>

                                <form method="POST" action="" id="login-form">

                                    <div class="form-group">
                                        <label for="useremail">Email :</label>
                                        <input type="email" class="form-control" id="useremail" required autofocus name="email"  placeholder="Email"   value="<?php echo !empty($user['email'])?$user['email']:''; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="PsLbl" for="Password">Password : <span class="pHints" data-toggle="tooltip"
                                                data-html="true" title="<div >Password Hints</div>
                                    <ul>
                                        <li>One lowercase character</li>
                                        <li>One uppercase character</li>
                                        <li>One number</li>
                                        <li>One special character</li>
                                        <li>8 characters minimum</li>

                                    </ul>">i</span>
                                            <span class="Pview"><img src="<?php echo base_url('assets');?>/images/eye.svg" /></span>
                                        </label>
                                        <input type="password" class="form-control" id="password" required name="password" placeholder="Password">
                                    </div>
                                        <?php
                                            if(!empty($this->form_validation->error_array())){
                                                $err = $this->form_validation->error_array();
                                                echo '<span class="text-danger">';
                                                foreach($err as $value){
                                                    echo $value.'<br>';
                                                }
                                                echo '</span>';
                                            }
                                        ?>
                                    <div class="row frmaxon">
                                        <div class="col">
                                            <a class="secondary-link transition" href="<?php echo base_url(); ?>users/login">Have an
                                                Account?</a>
                                        </div>
                                        <div class="col text-right">
                                        <input type="submit" style="border:none" class="loginButn btn bg-orrange" name="registerBtn"  value="Get Started"/>
                                        </div>
                                    </div>
                                </form>
                                <div class="ftrLog">
                                    By clicking the "Get Started!" button, you are creating a ASCEIC2020 account, and
                                    you agree to ASCEIC2020's <a href="#">Terms of Use</a> and <a href="#">Privacy
                                        Policy</a>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loginBnr">
                <div class="row">
                    <div class="col-lg-8 offset-lg-4">
                        <div class="parent-div addbox" style="background-image:url(<?php echo base_url('assets');?>/images/about.jpg);">
                            <div class="div-child">
                                <div class="addContent">
                                    <h1 class="addheader">The point of using Lorem Ipsum is very high</h1>
                                    <p>It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that it has a more-or-less normal distribution of letters.</p>
                                    <a href="#" class="btn transition" target="_blank">Click here</a>
                                </div>
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="<?php echo base_url('assets');?>/script/jquery-3.1.0.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="<?php echo base_url('assets');?>/bootstrap-4.3.1/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url('assets');?>/script/menu.js"></script>
            <script src="<?php echo base_url('assets');?>/script/particles.js"></script>
            <script src="<?php echo base_url('assets');?>/script/classie.js"></script>
            <script src="<?php echo base_url('assets');?>/script/script.js"></script>
            <script>
                $("body").on('click', '.Pview', function() {
                    
                    var input = $("#password");
                    if (input.attr("type") === "password") {
                        input.attr("type", "text");
                    } else {
                        input.attr("type", "password");
                    }

                });
            </script>
</body>

</html>