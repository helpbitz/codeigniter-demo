<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASCE India conference 2020</title>
    <link rel="shortcut icon" href="images/favcon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('assets');?>/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/setup.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/popup-page.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="particles-js"></div>
    <a class="btn btn-secondary popBack" href="#" onclick="history.back(-1)">Close</a>
    <div class="prelogin">
        <div class="container-fluid" style="padding:0;">
            <div class="row">
                <div class="col-lg-4">
                    <div class="parent-div" style="height:100vh; padding-bottom:50px;">
                        <div class="div-child">
                            <div class="loginBx">
                                <img src="<?php echo base_url('assets');?>/images/logo.png" />
                                <h1 class="mb-4 mt-4 pb-2 ">
                                    Logging to Proceed
                                </h1>
                                <div class="text-danger">
                                    <?php
                                        if(!empty($error_msg)){
                                            echo '<p class="statusMsg">'.$error_msg.'</p>';
                                        }
                                    ?>
                                </div>
                                <form method="POST" action="" id="login-form">

                                    <div class="form-group">
                                        <label for="username">Username :</label>
                                        <input type="email" name="email" class="form-control" id="username" placeholder="Email" required  value="<?php echo !empty($user['email'])?$user['email']:''; ?>" aria-describedby="usererror" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="Password">Password :</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                    </div>
                                    <div class="form-group text-danger">
                                        <?php
                                            if(!empty($this->form_validation->error_array())){
                                                $err = $this->form_validation->error_array();
                                                foreach($err as $value){
                                                    echo $value.'<br>';
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="row frmaxon">
                                        <div class="col">
                                            <a class="secondary-link transition" href="forgor-password.html">Forgot
                                                Password?</a>
                                        </div>
                                        <div class="col text-right">
                                        <input type="submit" id="logMeIn"  style="border:none" class="loginButn btn bg-orrange" name="loginBtn"  value="Login"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loginBnr">
                <div class="row">
                    <div class="col-lg-8 offset-lg-4">
                        <div class="parent-div addbox" style="background-image:url(<?php echo base_url('assets');?>/images/about.jpg);">
                            <div class="div-child">
                                <div class="addContent">
                                    <h1 class="addheader">The point of using Lorem Ipsum is very high</h1>
                                    <p>It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that it has a more-or-less normal distribution of letters.</p>
                                        <a href="<?php echo base_url(); ?>users/register" class="btn transition" target="_blank">Register Here</a>
                                </div>
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="<?php echo base_url('assets');?>/script/jquery-3.1.0.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="<?php echo base_url('assets');?>/bootstrap-4.3.1/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url('assets');?>/script/menu.js"></script>
            <script src="<?php echo base_url('assets');?>/script/particles.js"></script>
            <script src="<?php echo base_url('assets');?>/script/classie.js"></script>
            <script src="<?php echo base_url('assets');?>/script/script.js"></script>
</body>

</html>