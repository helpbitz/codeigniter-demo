<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASCE India conference 2020</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets');?>/images/favcon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('assets');?>/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/setup.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/popup-page.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="particles-js"></div>
    <div class="submit-paper">
        <div class="container-fluid" style="padding:0 3%;">
            <div class="row">
                <div class="col-12">
                    <a class="d-inline-block my-2" href="#"><img src="<?php echo base_url('assets');?>/images/logo.png" /> </a>
                    <h2 class="mt-4">Welcome to ASCE INDIA CONGRESS 2020</h2>
                    <div class="subhed">This is your page to manage your registration details. Go to My Dashboard
                        to
                        access your information, tickets, program schedule & abstracts.</div>
                    <div class="font-bold my-4">Allright let's set this up ! Tell us a bit about yourself to store in
                        our records for all the
                        future communications.</div>

                    <div class="mt-4 pt-4">
                        <div class="row">
                            <div class="col-12 col-lg-8">
                                <form class="customFrom" method="post" action="">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="lbl" for="exampleInputEmail1">Email address</label>
                                                <input type="email" name="email"  placeholder="Email"  value="<?php echo $email; ?>" class="inpt form-control" id="exampleInputEmail1" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="form-group">
                                                <label class="lbl" for="firstName">First Name</label>
                                                <input type="text" name="first-name" class="inpt form-control" id="firstName">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6  col-md-4">
                                            <div class="form-group">
                                                <label class="lbl" for="lastName">Last Name</label>
                                                <input type="text" name="last-name" class="inpt form-control" id="lastName">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6  col-md-4">
                                            <div class="form-group">
                                                <label class="lbl" for="Designation">Designation</label>
                                                <input type="text" name="designation" class="inpt form-control" id="Designation">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6  col-md-4">
                                            <div class="form-group">
                                                <label class="lbl" for="Organisation">Organisation</label>
                                                <input type="text" name="organization" class="inpt form-control" id="Organisation">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="lbl" for="address1">Address Line 1</label>
                                                <textarea class="inpt form-control" name="address-line-1" id="address1"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label class="lbl" for="address2">Address Line 2</label>
                                                <textarea class="inpt form-control" name="address-line-2" id="address2"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6  col-md-3">
                                            <div class="form-group">
                                                <label class="lbl" for="post">Post Code</label>
                                                <input type="text" class="inpt form-control" name="postcode" id="post">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6  col-md-3">
                                            <div class="form-group">
                                                <label class="lbl" for="city">City</label>
                                                <input type="text" name="city" class="inpt form-control" id="city">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6  col-md-3">
                                            <div class="form-group">
                                                <label class="lbl" for="State">State</label>
                                                <input type="text" name="state" class="inpt form-control" id="State">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6  col-md-3">
                                            <div class="form-group">
                                                <label class="lbl" for="Country">Country</label>
                                                <select class="inpt form-control" name="country" id="Country">
                                                    
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="form-group">
                                                <label class="lbl" for="Mobile">Mobile</label>
                                                <input type="text" class="inpt form-control" name="phone" id="Mobile">
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="form-group">
                                                <label class="lbl" for="fixedPhone">Fixed Phone</label>
                                                <input type="text" class="inpt form-control" id="fixedPhone">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="animateBtn mt-4" name="paperSubmitBtn" value="Save my information">
                                </form>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="light-blue helpBox" style="padding:20px; margin-bottom:15px;">
                                    <h4 class="mb-4">How to use ASCEIC2020 portal</h4>
                                    <div class="font-bold mb-1">Update Profile</div>
                                    <p>Update your registration information and change password.</p>

                                    <div class="font-bold mb-1">My Abstracts</div>
                                    <p>Access this menu to submit abstracts and manage abstract(s) you have already
                                        submitted.</p>

                                    <div class="font-bold mb-1">Tickets & Invoices</div>
                                    <p>Go to this page to complete your pending payments, if any, and to download and
                                        print invoices for fees paid for registration.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <script src="<?php echo base_url('assets');?>/script/jquery-3.1.0.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="<?php echo base_url('assets');?>/bootstrap-4.3.1/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url('assets');?>/script/menu.js"></script>
            <script src="<?php echo base_url('assets');?>/script/particles.js"></script>
            <script src="<?php echo base_url('assets');?>/script/classie.js"></script>
            <script src="<?php echo base_url('assets');?>/script/script.js"></script>
            <script>
          
                let dropdown = $('#Country');

                dropdown.empty();

                dropdown.append('<option selected="true" disabled>Choose Country</option>');
                dropdown.prop('selectedIndex', 0);

                const url = 'https://restcountries.eu/rest/v2/all';

                // Populate dropdown with list of provinces
                $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    dropdown.append($('<option></option>').attr('value', entry.name).text(entry.name));
                })
                });
            
            </script>
</body>

</html>