<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASCE India conference 2020</title>
    <link rel="shortcut icon" href="images/favcon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('assets');?>/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/setup.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/popup-page.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="particles-js"></div>
    <div class="reg2 text-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <a class="d-block my-4" href="#"><img src="<?php echo base_url('assets');?>/images/logo.png" /> </a>
                    <img class="py-4" style="max-width:100px;" src="<?php echo base_url('assets');?>/images/satisfied.svg" />
                    <h1>Congratulations !!</h1>
                    <div class="text-3xl">You have succefully validated your email address.</div>
                    <div class="mt-4 py-4 font-bold text-2xl">Continue to</div>
                    <a class="bigBtn animateBtn" href="<?php echo base_url().'index.php/users/submit-paper/'.$str;?>">SUBMIT PAPER FOR CONFERENCE</a>
                    <a class="bigBtn animateBtn bg-purple" href="#">REGISTER FOR CONFERENCE</a>
                </div>
            </div>


            <script src="<?php echo base_url('assets');?>/script/jquery-3.1.0.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="<?php echo base_url('assets');?>/bootstrap-4.3.1/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url('assets');?>/script/menu.js"></script>
            <script src="<?php echo base_url('assets');?>/script/particles.js"></script>
            <script src="<?php echo base_url('assets');?>/script/classie.js"></script>
            <script src="<?php echo base_url('assets');?>/script/script.js"></script>
</body>

</html>