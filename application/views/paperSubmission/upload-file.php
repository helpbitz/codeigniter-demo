<!--  Loading the footer template :: start -->
<?php  $this->load->view('templates/header2'); ?>
    <!--  Loading the footer template  :: end -->
    <!--=========== page start =============-->
    <div class="page-wrap">


        <div class="pad-top sidepad">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="pagetitleWrap">
                            <h1>Start your New Submissin on the theme</h1>
                            <div class="subhed">Challenges of Resilient and Sustainable Infrastructure Development in
                                Emerging Economies</div>
                        </div>
                        <div class="formNav">
                            <div class="navListWrp text-2xl">
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/paper-information/<?php echo $paper_id; ?>">1. Enter Paper Information</a>
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/add-co-author/<?php echo $paper_id; ?>">2. Add Co Author</a>
                                <a class="navTitle active" href="#">3. Upload Files</a>
                                <a class="navTitle <?php if($step >= 4) echo 'submited'; ?>" href="<?php if($step >= 4) echo base_url().'paper-submission/add-reviewer/'. $paper_id; ?>">4. Suggest Reviewers</a>
                                <a class="navTitle <?php if($step >= 5) echo 'submited'; ?>" href="<?php if($step >= 5) echo base_url().'paper-submission/review-submit/'. $paper_id; ?>">5. Review and Submit</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="pageContent">
                            <h3 class="mb-4">3. Upload Files</h3>
                            <form class="customFrom" enctype="multipart/form-data" method="post" action="">
                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <label class="lbl" id="fileName" for="Organisation">File Name</label>
                                        <div class="uploadBx">
                                            <div class="uploadArea mt-2">
                                                <label for="file">
                                                    <img src="<?php echo base_url();?>assets/images/profile.svg" />
                                                    <div class="fileInfo hideondrop">Upload a file</div>
                                                    <div class="fileInfo dropname"></div>
                                                    <input type="file" id="file" name="file" accept=".pdf" required>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-8">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label class="lbl" for="fileType">File type</label>
                                                    <?php 
                                                        echo '<select class="inpt form-control select2" id="topic" name="file_type" required>';
                                                        if(!empty($file_types)){
                                                            
                                                            foreach($file_types as $file_type){
                                                                $str =  '<option value="'.$file_type->id.'">'.$file_type->file_type.'</option>';   
                                                                echo $str; 
                                                            }
                                                            
                                                        }
                                                        else{
                                                            echo '<option>No Type Found!</option>';
                                                        }
                                                        echo '</select>';
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label class="lbl" for="Description">Description</label>
                                                    <textarea class="inpt form-control" name="description" id="Description" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <input type="submit" class="mt-4 btn animateBtn Btnsml bg-purple" id="uploadFile" name="uploadFile" value="Add File">
                                        </form>
                                    <form method="post" action="" id="fileForm">
                                        <?php if(!empty($files)){ ?>
                                            <table class="tableformob table">
                                                <thead>
                                                    <tr>
                                                        <th data-breakpoints="sm md">File Name</th>
                                                        <th>File Type</th>
                                                        <th data-breakpoints="sm md">Description</th>
                                                        <!-- <th data-breakpoints="sm md">Status</th> -->
                                                        <th data-type="html">action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="sortable">
                                                <?php foreach($files as $file){ ?>
                                                    <tr id="<?php echo $file->id;?>">
                                                        <td><?php echo $file->file_name; ?></td>
                                                        <td><?php echo $file->file_type; ?></td>
                                                        <td><?php echo $file->description; ?></td>
                                                        <!-- <td>active</td> -->
                                                        <td>
                                                            <div class="btn-group">
                                                                <a href="<?php  echo base_url()?>uploads/<?php echo $file->file_name; ?>" class="btn btn-tbl">Download</a>
                                                                
                                                                <button class="btn btn-tbl" name="deleteFile" id="deleteFile" value="<?php echo $file->id; ?>">Delete</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php 
                                            }    
                                        ?>
                                    </div>
                                    </div>
                                    <input type="hidden" id="base" value="<?php echo base_url(); ?>">
                                    <input type="hidden" name="sortFileOrders">
                                    <div class="mt-4 pt-4 btn-toolbar">
                                        <div class="btn-group mr-4" role="group">
                                            <input type="submit" class="btn animateBtn" name="sortFileBtn" id="sortFileBtn" value="Save">
                                        </div>
                                        <div class="btn-group" role="group">
                                            <input type="submit" class="btn animateBtn bg-purple" name="sortFileBtn2" id="saveUploadFilesBtn" value="Save and Add Reviewer">
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="light-blue helpBox" style="padding:20px; margin-bottom:15px;">
                            <h3>Help</h3>
                            <div class="">
                                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out
                                    print, graphic or web designs. The passage is attributed to an unknown typesetter
                                    in the 15th century.</p>
                                <ul>
                                    <li>lipsum as it is sometimes known</li>
                                    <li>he passage is attributed to an unknown</li>
                                    <li>graphic or web designs. The passage is attributed to an unknown</li>
                                    <li>dummy text used in laying out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!--  Loading the footer template :: start -->
    <?php  $this->load->view('templates/footer'); ?>
    <!--  Loading the footer template  :: end -->