<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    </style>
</head>
<body>
<h1>Paper information</h1>
<table style="width:100%">
  <tr>
    <th>Title</th>
    <th>Abstruct</th>
    <th>Keywords</th>
    <th>Topic</th>
    <th>Sub Theme</th>
  </tr>
  <tr>
    <td><?php echo $allData['paper_info']->title; ?></td>
    <td><?php echo $allData['paper_info']->abstruct; ?></td>
    <td><?php echo $allData['paper_info']->keywords; ?></td>
    <td><?php echo $allData['paper_info']->topic; ?></td>
    <td><?php echo $allData['paper_info']->sub_theme; ?></td>
  </tr>
</table> 
<h1>Co-author information</h1>
<table style="width:100%">
  <tr>
    <th>Co-author email</th>
    <th>Co-author name</th>
    <th>Co-author designation</th>
    <th>Co-author organization</th>
    <th>Co-author image</th>
  </tr>
  <?php foreach($allData['co_author'] as $data){?>
  <tr>
    <td><?php echo $data->email; ?></td>
    <td><?php echo $data->name; ?></td>
    <td><?php echo $data->designation; ?></td>
    <td><?php echo $data->organization; ?></td>
    <td><img src="<?php echo base_url().'uploads/'.$data->photo_url; ?>" alt="" style="height:100px; width:100px"></td>
  </tr>
  <?php }?>
</table> 
</body>
</html>