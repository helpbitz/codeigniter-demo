<!--  Loading the footer template :: start -->
<?php  $this->load->view('templates/header2'); ?>
    <!--  Loading the footer template  :: end -->
    <!--=========== page start =============-->
    <div class="page-wrap">


        <div class="pad-top sidepad">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="pagetitleWrap">
                            <h1>Start your New Submissin on the theme</h1>
                            <div class="subhed">Challenges of Resilient and Sustainable Infrastructure Development in
                                Emerging Economies</div>
                        </div>
                        <div class="formNav">
                            <div class="navListWrp text-2xl">
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/paper-information/<?php echo $paper_id; ?>">1. Enter Paper Information</a>
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/add-co-author/<?php echo $paper_id; ?>">2. Add Co Author</a>
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/upload-file/<?php echo $paper_id; ?>">3. Upload Files</a>
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/add-reviewer/<?php echo $paper_id; ?>">4. Suggest Reviewers</a>
                                <a class="navTitle active" href="#">5. Review and Submit</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="pageContent">
                            <h3 class="mb-4">5. Review and Submit</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus
                                mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>
                            <form action="" method="post">
                                <input type="submit" name="downloadPdf" class="btn animateBtn" value="Download & Review Submission">
                            </form>
                           

                            <form class="customFrom" method="post" action="">
                                <div class="row">
                                    <div class="col-12 mt-4 pt-4">
                                        <h3>Submission Files</h3>
                                        <?php if(!empty($files)){ ?>
                                            <table class="tableformob table mb-4">
                                                <thead>
                                                    <tr>
                                                        <th data-breakpoints="sm md">File Name</th>
                                                        <th>File Type</th>
                                                        <th data-breakpoints="sm md">Displayed in PDF ?</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($files as $file){ ?>
                                                    <tr>
                                                        <td><?php echo $file->file_name; ?></td>
                                                        <td><?php echo $file->file_type; ?></td>
                                                        <td>Yes</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                                            bibendum
                                            laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo
                                            commodo. Proin
                                            sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis
                                            parturient montes,
                                            nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate,
                                            felis tellus
                                            mollis orci, sed rhoncus pronin sapien nunc accuan eget.</p>

                                        <div class="checkBox mt-4">
                                            <input type="checkbox" id="checkbox-2" required>
                                            <label for="checkbox-2">*I have received the PDF preview of my submission
                                                and approve it for the consideration by the commitee</label>
                                        </div>
                                        <div class="checkBox mt-2">
                                            <input type="checkbox" id="checkbox-2" required>
                                            <label for="checkbox-2">*I have read and adhere to Etyhics in <a href="#">publishing
                                                    guideline</a>publishing guideline</label>
                                        </div>

                                    </div>
                                </div>

                                <input type="submit" class="mt-4 btn animateBtn" name="completeSubmission" value="Complete Submission">
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="light-blue helpBox" style="padding:20px; margin-bottom:15px;">
                            <h3>Help</h3>
                            <div class="">
                                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out
                                    print, graphic or web designs. The passage is attributed to an unknown typesetter
                                    in the 15th century.</p>
                                <ul>
                                    <li>lipsum as it is sometimes known</li>
                                    <li>he passage is attributed to an unknown</li>
                                    <li>graphic or web designs. The passage is attributed to an unknown</li>
                                    <li>dummy text used in laying out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!--  Loading the footer template :: start -->
    <?php  $this->load->view('templates/footer'); ?>
    <!--  Loading the footer template  :: end -->