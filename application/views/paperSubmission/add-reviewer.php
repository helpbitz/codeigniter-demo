    <!--  Loading the footer template :: start -->
    <?php  $this->load->view('templates/header2'); ?>
    <!--  Loading the footer template  :: end -->
    <!--=========== page start =============-->
    <!--=========== page start =============-->
    <div class="page-wrap">


        <div class="pad-top sidepad">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="pagetitleWrap">
                            <h1>Start your New Submissin on the theme</h1>
                            <div class="subhed">Challenges of Resilient and Sustainable Infrastructure Development in
                                Emerging Economies</div>
                        </div>
                        <div class="formNav">
                            <div class="navListWrp text-2xl">
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/paper-information/<?php echo $paper_id; ?>">1. Enter Paper Information</a>
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/add-co-author/<?php echo $paper_id; ?>">2. Add Co Author</a>
                                <a class="navTitle submited" href="<?php echo base_url();?>paper-submission/upload-file/<?php echo $paper_id; ?>">3. Upload Files</a>
                                <a class="navTitle active" href="#">4. Suggest Reviewers</a>
                                <a class="navTitle <?php if($step >= 5) echo 'submited'; ?>" href="<?php if($step >= 5) echo base_url().'paper-submission/review-submit/'. $paper_id; ?>">5. Review and Submit</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="pageContent">
                            <h3 class="mb-4">4. Suggest Reviewers</h3>
                            <form class="customFrom" id="customFrom" method="post" action="" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-12 col-lg-8">
                                        <div class="row">
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label class="lbl" for="firstName">Email</label>
                                                    <input type="email" name="reviewer_email" class="inpt form-control" id="reviewer_email">
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label class="lbl" for="lastName">Name</label>
                                                    <input type="text" class="inpt form-control" id="reviewer_name" name="reviewer_name">
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label class="lbl" for="Designation">Designation</label>
                                                    <input type="text" class="inpt form-control" id="reviewer_desig" name="reviewer_desig">
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label class="lbl" for="Organisation">Organisation</label>
                                                    <input type="text" class="inpt form-control" id="reviewer_org" name="reviewer_org">
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6">
                                                <div class="form-group">
                                                    <label class="lbl" for="Country">Country</label>
                                                   <!--  <input type="text" class="inpt form-control" id="reviewer_country" name="reviewer_country"> -->
                                                    <select class="inpt form-control select2" name="reviewer_country" id="reviewer_country">
                                                    
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label class="lbl" for="Note">Note</label>
                                                    <textarea class="inpt form-control" id="reviewer_note" name="reviewer_note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <label class="lbl" for="Organisation">Upload Photo</label>
                                        <div class="uploadBx">
                                            <div class="uploadArea mt-2">
                                                <label for="file">
                                                    <img src="<?php echo base_url(); ?>assets/images/profile.svg" />
                                                    <div class="fileInfo hideondrop"><button class="addPhoto">Choose photo</button></div>
                                                    <div class="fileInfo dropname"></div>
                                                    <!-- <input type="file" id="choose" name="files[]" multiple="multiple" accept=".jpeg, .png, .jpg" > -->
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 optionBox">
                                        <div class="block row">
                                            <span class="add_reviewer">
                                                <button type="button" class="btn animateBtn Btnsml bg-purple">Add Reviewer</button>
                                            </span>
                                        </div>
                                        <div class="row block">
                                        <?php 
                                        if(!empty($reviewers)){
                                            foreach($reviewers as $reviewer){
                                        ?>
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-5" id="reviewers">
                                            <div class="InfoBox authorBx my-4">
                                                <img src="<?php echo base_url(); ?>uploads/<?php echo $reviewer->photo_url; ?>" />
                                                <div class="text-2xl mt-2 font-bold"><?php echo $reviewer->name; ?></div>
                                                <div class=""><?php echo $reviewer->email; ?></div>
                                                <div class="mt-2 font-bold"><?php echo $reviewer->designation; ?></div>
                                                <div class=""><?php echo $reviewer->organization; ?></div>
                                            </div>
                                        </div>
                                        <?php
                                            }
                                        } 
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="reviewer_data" id="hidden_input">
                                <input type="hidden" name="paper_id" value="<?php echo $paper_id; ?>">
                                <div class="mt-4 pt-4 btn-toolbar">
                                    <div class="btn-group mr-4" role="group">
                                        <input type="submit" class="btn animateBtn save_btn" value="Save" name="SaveBtn">
                                    </div>
                                    <div class="btn-group" role="group">
                                        <input type="submit" class="btn animateBtn bg-purple save_btn"  value="Save and Submit after Review" name="SaveBtn2">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="light-blue helpBox" style="padding:20px; margin-bottom:15px;">
                            <h3>Help</h3>
                            <div class="">
                                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out
                                    print, graphic or web designs. The passage is attributed to an unknown typesetter
                                    in the 15th century.</p>
                                <ul>
                                    <li>lipsum as it is sometimes known</li>
                                    <li>he passage is attributed to an unknown</li>
                                    <li>graphic or web designs. The passage is attributed to an unknown</li>
                                    <li>dummy text used in laying out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!--  Loading the footer template :: start -->
    <?php  $this->load->view('templates/footer'); ?>
    <!--  Loading the footer template  :: end -->