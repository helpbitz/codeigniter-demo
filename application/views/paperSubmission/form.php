    <!--  Loading the footer template :: start -->
        <?php  $this->load->view('templates/header2'); ?>
    <!--  Loading the footer template  :: end -->
    <!--=========== page start =============-->
    <div class="page-wrap">


        <div class="pad-top sidepad">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="pagetitleWrap">
                            <h2>Start your New Submission on the theme</h2>
                            <div class="subhed">Challenges of Resilient and Sustainable Infrastructure Development in
                                Emerging Economies</div>
                        </div>
                        <div class="formNav">
                            <div class="navListWrp text-2xl">
                                <a class="navTitle active" href="#">1. Enter Paper Information</a>
                                <a class="navTitle" href="#">2. Add Co Author</a>
                                <a class="navTitle" href="#">3. Upload Files</a>
                                <div class="navTitle" href="#">4. Suggest Reviewers</div>
                                <a class="navTitle" href="#">5. Review and Submit</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="pageContent">
                            <form>
                               <!--  <div class="form-group">
                                    <label for="exampleFormControlInput1">Email address</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                                </div> -->
                                <div class="form-group">
                                    <label for="sub-theme">Please Select a Sub Theme</label>
                                    <select class="form-control" id="sub-theme" name="sub-theme">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="topic">Please Select a Topic</label>
                                    <select class="form-control" id="topic" name="topic">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="title">Full Title</label>
                                    <textarea class="form-control" id="title" rows="3" name="title"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="abstruct">Abstruct</label>
                                    <textarea class="form-control" id="abstruct" rows="3" name="abstruct"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="keyword">Keywords</label>
                                    <textarea class="form-control" id="keyword" rows="3" name="keywords"></textarea>
                                </div>
                                <div class="mt-4 pt-2 btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                    <div class="btn-group mr-4" role="group" aria-label="First group">
                                        <button type="button" class="btn btn-secondary">Save</button>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Second group">
                                        <button type="button" class="btn btn-primary">Save and Add Co Author</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="light-blue helpBox" style="padding:20px; margin-bottom:15px;">
                            <h3>Help</h3>
                            <div class="">
                                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out
                                    print, graphic or web designs. The passage is attributed to an unknown typesetter
                                    in the 15th century.</p>
                                <ul>
                                    <li>lipsum as it is sometimes known</li>
                                    <li>he passage is attributed to an unknown</li>
                                    <li>graphic or web designs. The passage is attributed to an unknown</li>
                                    <li>dummy text used in laying out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!--  Loading the footer template :: start -->
        <?php  $this->load->view('templates/footer'); ?>
    <!--  Loading the footer template  :: end -->