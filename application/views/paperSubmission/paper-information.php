    <!--  Loading the footer template :: start -->
    <?php  $this->load->view('templates/header2'); ?>
    <!--  Loading the footer template  :: end -->
    <?php 
    if(!empty($msg)){
        echo '<script language="javascript">';
        echo 'alert("'.$msg.'")';
        echo '</script>';
        header('Location:'.base_url().'paper-submission/paper-information/'.$paper_id);
    } 
    ?>
    <!--=========== page start =============-->
    <div class="page-wrap">
        <div class="pad-top sidepad">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="pagetitleWrap">
                            <h1>Start your New Submissin on the theme</h1>
                            <div class="subhed">Challenges of Resilient and Sustainable Infrastructure Development in
                                Emerging Economies</div>
                        </div>
                        <div class="formNav">
                            <div class="navListWrp text-2xl">
                                <a class="navTitle active" href="#">1. Enter Paper Information</a>
                                <a class="navTitle <?php if($step >= 2) echo 'submited'; ?>" href="<?php echo base_url();?>paper-submission/add-co-author/<?php if(isset($paper_info->id)) echo $paper_info->id; ?>">2. Add Co Author</a>
                                <a class="navTitle <?php if($step >= 3) echo 'submited'; ?>" href="<?php echo base_url();?>paper-submission/upload-file/<?php if(isset($paper_info->id)) echo $paper_info->id; ?>">3. Upload Files</a>
                                <a class="navTitle <?php if($step >= 4) echo 'submited'; ?>" href="<?php echo base_url();?>paper-submission/add-reviewer/<?php if(isset($paper_info->id)) echo $paper_info->id; ?>">4. Suggest Reviewers</a>
                                <a class="navTitle <?php if($step >= 5) echo 'submited'; ?>" href="<?php echo base_url();?>paper-submission/review-submit/<?php if(isset($paper_info->id)) echo $paper_info->id; ?>">5. Review and Submit</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-8">
                        <div class="pageContent">
                            <h3 class="mb-4">1. Enter Paper Information</h3>
                            <form class="customFrom" method="POST" action="" id="myForm">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="lbl" for="theme">Select a Sub Theme</label>
                                            <?php 
                                                echo '<select class="inpt select2 form-control" id="theme" name="sub_theme" required><option selected disabled>-Please Select a Sub Theme-</option>';
                                                if(!empty($sub_themes)){
                                                    
                                                    foreach($sub_themes as $sub_theme){
                                                        
                                                        $str =  '<option value="'.$sub_theme->id.'"';
                                                        if(!empty($paper_info))
                                                            if($paper_info->sub_theme_id == $sub_theme->id)
                                                                $str .= 'selected';
                                                        $str .= '>'.$sub_theme->sub_theme.'</option>';   
                                                        echo $str;
                                                    }
                                                    
                                                }
                                                else{
                                                    echo '<option>No Sub Theme Found!</option>';
                                                }
                                                echo '</select>';
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="lbl" for="topic">Select a Topic</label>
                                            <?php 
                                                echo '<select class="inpt form-control select2" id="topic" name="topic" required><option selected disabled>-Please Select a Topic-</option>';
                                                if(!empty($topics)){
                                                    
                                                    foreach($topics as $topic){
                                                        $str =  '<option value="'.$topic->id.'"';
                                                        if(!empty($paper_info))
                                                            if($paper_info->topic_id == $topic->id)
                                                                $str .= 'selected';
                                                        $str .= '>'.$topic->topic.'</option>';   
                                                        echo $str; 
                                                    }
                                                    
                                                }
                                                else{
                                                    echo '<option>No Topic Found!</option>';
                                                }
                                                echo '</select>';
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="lbl" for="title">Full Title</label>
                                            <textarea class="inpt form-control" id="title" name="title" required>   
                                                <?php
                                                    if(!empty($paper_info))
                                                        echo $paper_info->title;
                                                ?>
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="lbl" for="Abstruct">Abstruct</label>
                                            <textarea class="inpt form-control Abstruct" id="Abstruct" name="abstruct" required>
                                            <?php
                                                if(!empty($paper_info))
                                                    echo $paper_info->abstruct;
                                            ?>
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="lbl" for="Keywords">Keywords</label>
                                            <textarea class="inpt form-control" id="Keywords" name="keywords" required>
                                            <?php
                                                if(!empty($paper_info))
                                                    echo $paper_info->keywords;
                                            ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-4 pt-2 btn-toolbar">
                                    <div class="btn-group mr-4" role="group">
                                        <input type="submit" name="SaveBtn" class="btn animateBtn bg-purple"id="save" value="Save">
                                    </div>
                                    <div class="btn-group" role="group">
                                        <input type="submit" id="submitBtn" class="btn animateBtn" name="SaveBtn2" value="Save and Add Co Author">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="light-blue helpBox" style="padding:20px; margin-bottom:15px;">
                            <h3>Help</h3>
                            <div class="">
                                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out
                                    print, graphic or web designs. The passage is attributed to an unknown typesetter
                                    in the 15th century.</p>
                                <ul>
                                    <li>lipsum as it is sometimes known</li>
                                    <li>he passage is attributed to an unknown</li>
                                    <li>graphic or web designs. The passage is attributed to an unknown</li>
                                    <li>dummy text used in laying out</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!--  Loading the footer template :: start -->
    <?php  $this->load->view('templates/footer'); ?>
    <!--  Loading the footer template  :: end -->