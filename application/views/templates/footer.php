<footer class="sidepad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-3">
                    <img class="ftrimg" src="<?php echo base_url('assets');?>/images/logo.png" />
                    <p class="srtAbout">Choose the ASCE IC 2020 registration category that best fits your schedule and
                        budget, and register now! For maximum savings, be sure to register before 02 march, 2020.<a
                            href="http://www.asceic2020.org/about-us">Read more..</a></p>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 col-xl-2 offset-xl-1">
                    <div class="ftrHdr">Services</div>
                    <ul>
                        <li><a href="http://www.asceic2020.org/conference">conference</a></li>
                        <li><a href="http://www.asceic2020.org/submissions">submissions</a></li>
                        <li><a href="#">Registration</a></li>
                        <li><a href="#">Sponsor</a></li>
                        <li><a href="http://www.asceic2020.org/about-us">Organizer</a></li>
                    </ul>
                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="ftrHdr">Contact</div>
                    <address>
                        <span class="bold">Adress:</span> 25A, Raja Basanta Roy Road, 1st floor<br />
                        Kolkata 700026<br />
                        West Bengal, India
                    </address>
                    <div class="mb-2">
                        <span class="bold">Email:</span> convenor@asceic2020.Org
                    </div>


                </div>
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="ftrHdr">Newsletter</div>
                    <form>
                        <input type="email" name="EMAIL" placeholder="Enter Your Email *" required="">
                        <button type="submit" class="btn btn-block bg-orrange">Subscribe</button>
                    </form>
                </div>
            </div>
            <div class="row btmftr">
                <div class="col-12 col-sm-6">
                    <a href="">Terms of use</a> | <a href="">Privacy Policy</a>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="cpywrit">Copyright 2019 all rights reserved</div>
                </div>
                <div class="col-12 text-center mt-2">
                    <span class="developer">design
                        & Developed by <a target="_blank" href="http://mastiska.com/">Mastiska</a>.</span>
                </div>
            </div>
        </div>

    </footer>
    <script src="<?php echo base_url('assets');?>/script/jquery-3.1.0.min.js"></script>
    <script src="<?php echo base_url('assets');?>/script/jquery-ui.min.js"></script>
    <script src="<?php echo base_url('assets');?>/script/jquery.tagsinput.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="<?php echo base_url('assets');?>/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets');?>/script/menu.js"></script>
    <script src="<?php echo base_url('assets');?>/script/particles.js"></script>
    <script src="<?php echo base_url('assets');?>/script/classie.js"></script>
    <script src="<?php echo base_url('assets');?>/script/script.js"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        tinymce.init({
        mode : "specific_textareas",
        editor_selector : "Abstruct",
        });</script>
    <script>

        var arr = [];
        //Add co-author
        $('.add').click(function() {
            var email = $('#co_author_email').val();
            var name = $('#co_author_name').val();
            var desig = $('#co_author_designation').val();
            var org = $('#co_author_org').val();
            var isCoAuthor = $("#checkbox-1").is(":checked");
           
            if(email == "")
                $('#co_author_email').css('border-color','red').attr("placeholder", "Email is required");
            if(name == "")
                $('#co_author_name').css('border-color','red').attr("placeholder", "Name is required");
            if(desig == "")
                $('#co_author_designation').css('border-color','red').attr("placeholder", "Designation is required");
            if(org == '')
                $('#co_author_org').css('border-color','red').attr("placeholder", "Organization is required"); 
            if(email=='' || name=='' || desig=='' || org=='')
                return;  
                swal({
                    title: "Do You Want to Add More Authors?",
                    text: "",
                    icon: "success",
                    buttons: [
                        'No',
                        'Yes'
                    ],
                    }).then(function(isConfirm) {
                    if (isConfirm) {
                        $( "#co_author_email" ).focus();
                        $( "#co_author_email" ).val('');
                        $( "#co_author_name" ).val('');
                        $( "#co_author_designation" ).val('');
                        $( "#co_author_org" ).val('');
                    } else {
                        $("#saveUploadFilesBtn").focus();
                    }
                })  
            arr.push({email, name, desig, org, isCoAuthor});
            isCoAuthor ? isCoAuthor = 'checked disabled' : isCoAuthor ='';
            $('.block:last').append('<div class="col-12 col-sm-6 col-md-4 col-lg-5" id="reviewers"><div class="InfoBox authorBx my-4"><img src="<?php echo base_url(); ?>assets/images/profile.svg" /><div class="text-2xl mt-2 font-bold">'+name+'</div><div class="">'+email+'</div><div class="mt-2 font-bold">'+desig+'</div><div class="">'+org+'</div><input type="checkbox" '+isCoAuthor+'>Corresponding Author</div></div>');
            var str = JSON.stringify( arr );
            $('input[name="co_author_data"]').val(str);
            $('.save_btn').attr("disabled", false);
            
        });

        //Add reviewer
        $('.add_reviewer').click(function() {
            var email = $('#reviewer_email').val();
            var name = $('#reviewer_name').val();
            var desig = $('#reviewer_desig').val();
            var org = $('#reviewer_org').val();
            var country = $('#reviewer_country').val();
            var note = $('#reviewer_note').val();
           
            if(email == "")
                $('#reviewer_email').css('border-color','red').attr("placeholder", "Email is required");
            if(name == "")
                $('#reviewer_name').css('border-color','red').attr("placeholder", "Name is required");
            if(desig == "")
                $('#reviewer_desig').css('border-color','red').attr("placeholder", "Designation is required");
            if(org == '')
                $('#reviewer_org').css('border-color','red').attr("placeholder", "Organization is required"); 
            if(country == '')
                $('#reviewer_country').css('border-color','red').attr("placeholder", "Country is required");
            if(note == '')
                $('#reviewer_note').css('border-color','red').attr("placeholder", "Note is required");
            if(email == "" || name == "" || desig== "" || org=="" || country=="" || note=="")
                return;
            arr.push({email, name, desig, org,country,note});
            swal({
                    title: "Do You Want to Add More Reviewers?",
                    text: "",
                    icon: "success",
                    buttons: [
                        'No',
                        'Yes'
                    ],
                    }).then(function(isConfirm) {
                    if (isConfirm) {
                        $( "#reviewer_email" ).focus();
                        $( "#reviewer_email" ).val('');
                        $( "#reviewer_name" ).val('');
                        $( "#reviewer_desig" ).val('');
                        $( "#reviewer_org" ).val('');
                        $( "#reviewer_country" ).val('');
                        $( "#reviewer_note" ).val('');
                    } else {
                        $("#saveReviewBtn").focus();
                    }
                })  
            
            $('.block:last').append('<div class="col-12 col-sm-6 col-md-4 col-lg-5" id="reviewers"><div class="InfoBox authorBx my-4"><img src="<?php echo base_url(); ?>assets/images/profile.svg" />                 <div class="text-2xl mt-2 font-bold">'+name+'</div><div class="">'+email+'</div><div class="mt-2 font-bold">'+desig+'</div><div class="">'+org+'</div></div></div>');
            var str = JSON.stringify( arr );
            $('input[name="reviewer_data"]').val(str);
            $('.save_btn').attr("disabled", false);
        });
       
        if($('#reviewers').length == 0)
            $('.save_btn').attr("disabled", true);
       
        $(document).ready(function() {
            $('.select2').select2();


            $('#theme').change(function() {
                var id = $(this).val();
                
                $.ajax({url: "<?php echo base_url();?>paperSubmission/getTopic/"+id, success: function(result){                    
                   var result =JSON.parse(result);
                   var options = "<option selected disabled>-Please Select a Topic-</option>";
                   result.map(function(value, key){
                       options += '<option value="'+value.id+'">'+value.topic+'<option>';
                   })
                   $('#topic').html(options);    
                }});
            });


            //Multiple upload for co author
            $('form button.addPhoto').click(function(e) {
                e.preventDefault();
                var nb_attachments = $('form input').length;
                var $input = $('<input type="file" name=files[] id="file" multiple>');
                $input.on('change', function(evt) {
                    var f = evt.target.files[0];
                    $('form').append($(this));
                });
                $input.hide();
                $input.trigger('click');
                
            });

            //sortable list order
            $( ".sortable" ).sortable({
                stop: function(event, ui) {
                var itemOrder = $('.sortable').sortable("toArray");
                var arr = [];
                    for (var i = 0; i < itemOrder.length; i++) {
                        arr.push({"sequence":i,"id":itemOrder[i]});
                    }
                console.log(arr);
                $('input[name="sortFileOrders"]').val(JSON.stringify(arr));
                }
            });

            //jquery tags
            $('#Keywords').tagsInput({'width':'100%','defaultText':''});

            //populate countries
                let dropdown = $('#reviewer_country');

                dropdown.empty();

                dropdown.append('<option selected="true" disabled>Choose Country</option>');
                dropdown.prop('selectedIndex', 0);

                const url = 'https://restcountries.eu/rest/v2/all';

                // Populate dropdown with list of provinces
                $.getJSON(url, function (data) {
                $.each(data, function (key, entry) {
                    dropdown.append($('<option></option>').attr('value', entry.name).text(entry.name));
                })
                });

                //Show file name
                $('#file').change(function(){
                    var fileName = $(this).val().split('\\').pop();
                    $('#fileName').html(fileName);
                });
                //Submit form delete file
                $('#deleteFile').click(function(){
                    
                    $('form#fileForm').submit();
                })
        });

    </script>
</body>

</html>