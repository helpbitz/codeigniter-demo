<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASCE India conference 2020</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets');?>/images/favcon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('assets');?>/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/setup.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/page.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets');?>/style/jquery.tagsinput.css" />
    <style>
    div.tagsinput{
        background: rgba(255, 255,255, 0.15) !important;
        border: 1px solid rgba(9, 16, 76, 0.45);
        font-weight: 400;
        color: #09104c;
        display: block;
    }
    div.tagsinput span.tag {
        background: rgba(255, 255,255, 0.15) !important;
        border: 1px solid rgba(9, 16, 76, 0.45);
        font-weight: 400;
        color: #09104c;
    }
    </style>
</head>

<body>
    <div id="particles-js"></div>
    <header class="sidepad">
        <div class="container-fluid">
            <div class="row" style="position:relative;">
                <div class="col-6 col-md-3 logoFx">
                    <a href="<?php echo base_url(); ?>" class="logoWrap transition">
                        <img class="transition" src="<?php echo base_url('assets');?>/images/logo.png" />
                    </a>
                </div>
                <div class="col-12 col-md-9 mnuwrp">
                    <div id='cssmenu'>
                        <ul>
                            <li>
                                <span class="authrNm">Welcome <?php echo $name ?>! As</span>
                                <div class="btn-group mnudrop ml-sm-2  mr-sm-4">
                                    <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <?php echo $role; ?>
                                    </button>
                                    <!-- <div class="dropdown-menu dropdown-menu-right">
                                        <button class="dropdown-item py-2" type="button">Reviewer</button>
                                        <button class="dropdown-item py-2" type="button">Co Author</button>
                                    </div> -->
                                </div>
                            </li>
                            <li>
                                <div class="btn-group mnudrop  ml-sm-4">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Profile
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="#" class="dropdown-item px-4 py-2">Edit Profile</a>
                                        <a href="#" class="dropdown-item px-4 py-2">Change Password</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="<?php echo base_url('users/logout') ?>" class="dropdown-item px-4 py-2">Log out</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </header>