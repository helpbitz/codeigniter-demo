<!DOCTYPE html>
<html lang="en">

<head>
    <title>ASCE India conference 2020</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets');?>/images/favcon.png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url('assets');?>/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets');?>/style/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="particles-js"></div>
    <header class="sidepad">
        <div class="container-fluid">
            <div class="row" style="position:relative;">
                <div class="col-6 col-md-3 logoFx">
                    <a href="<?php echo base_url();?>" class="logoWrap transition">
                        <img class="transition" src="<?php echo base_url('assets');?>/images/logo.png" />
                    </a>
                </div>
                <div class="col-12 col-md-9 mnuwrp">
                    <div id='cssmenu'>
                        <ul>
                            <li><a class="active" href="<?php echo base_url();?>">Home</a></li>
                            <li class="has-submenu">
                                <a href="#">Conference</a>
                                <ul class="sub-menu">
                                    <li><a href="#">About
                                            Conference</a></li>
                                    <li><a href="#">Programmes</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Submissions</a></li>
                            <li><a href="#">Registration</a></li>
                            <li><a href="#">Sponsor</a></li>
                            <li><a href="#">Organizer</a></li>


                            <li class="visible-sml"><a class="btn btn-secondary mnuBtn loginBtn" href='#'>Login</a></li>
                            <!--<div class="btn-group mnudrop">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    Profile
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button class="dropdown-item py-2" type="button">View Profile</button>
                                    <button class="dropdown-item py-2" type="button">Change password</button>
                                    <div class="dropdown-divider"></div>
                                    <button class="dropdown-item py-2" type="button">Log out</button>
                                </div>
                            </div>-->
                        </ul>
                    </div>
                    <a class="has-right loginBtn transition" href='<?php echo base_url(); ?>users/login'>Login</a>
                    <a class="actnBtn bg-orrange pulse" href='<?php echo base_url(); ?>users/register'>Register</a>
                </div>
            </div>
        </div>
    </header>