    <!--  Loading the footer template :: start -->
        <?php  $this->load->view('templates/header2'); ?>
    <!--  Loading the footer template  :: end -->
    <!--=========== Page start =============-->
    <div class="page-wrap">


        <div class="pad-top sidepad">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="pagetitleWrap">
                            <h1 class="hedTitle">My Abstracts</h1>
                            <a class="animateBtn" href="<?php echo base_url(); ?>paper-submission/paper-information">New Submission</a>
                        </div>
                        <div class="pageContent">
                            <table class="tableformob table">
                                <thead>
                                    <tr>
                                        <th data-breakpoints="sm md">Num</th>
                                        <th>Abstract ID</th>
                                        <th data-breakpoints="sm md">Abstract Titile</th>
                                        <th data-breakpoints="sm md">Abstract Topic</th>
                                        <th data-breakpoints="xs sm md">Status</th>
                                        <th data-breakpoints="sm md">Payment</th>
                                        <th data-breakpoints="sm md">Submitted On</th>
                                        <th data-type="html">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($papers)) { $i=1;  ?>
                                    <tr>
                                    <?php  foreach($papers as $paper){ ?>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $paper->id; ?></td>
                                        <td><?php echo $paper->title; ?></td>
                                        <td><?php echo $paper->topic; ?></td>
                                        <td><?php echo $paper->status; ?></td>
                                        <td><?php if($paper->payment_status){echo 'Yes';}else{echo 'No';} ?></td>
                                        <td><?php if($paper->status !='review') {echo 'N/A';} else {echo $paper->updated_on;} ?></td>
                                        <td>
                                            <a href="<?php echo base_url();?>/paper-submission/redirect-to-step/<?php echo $paper->id; ?>" class="btn btn-tbl<?php if($paper->status != 'pending') echo ' disabled'; ?>">Edit</a>
                                        </td>
                                    </tr>
                                    <?php 
                                            }
                                    }else{ ?>
                                    <tr>
                                        <td colspan="6" style="text-align:center;">No Data Found</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!---->
                    </div>
                </div>

            </div>
        </div>



    </div>
    <!--=========== partner start =============-->
   <!--  Loading the footer template :: start -->
   <?php  $this->load->view('templates/footer'); ?>
  <!--  Loading the footer template  :: end -->