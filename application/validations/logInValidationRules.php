<?php


$rules = array(
			
    [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|trim'
    ],
    [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[8]|callback_password_check',
                'errors'=> array('password_check' => 'Invalid password format')
    ]

 );