<?php


$rules = array(
			
    [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|is_unique[opss_user.email]|trim',
                'errors'=> array('is_unique' => 'User Already Exists')
    ],
    [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[8]|callback_password_check',
                'errors'=> array('password_check' => 'Invalid password format')
    ]

 );