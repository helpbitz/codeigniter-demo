<?php


$rules = array(
			
    [
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'required|regex_match[/^[a-zA-Z\s]+$/]|trim',
    ],
    [
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'required|regex_match[/^[a-zA-Z\s]+$/]|trim'
    ],
    [
                'field' => 'designation',
                'label' => 'Designation',
                'rules' => 'required|trim'
    ],
    [
                'field' => 'organization',
                'label' => 'Organization',
                'rules' => 'required|trim'
    ],
    [
                'field' => 'address-line-1',
                'label' => 'Address Line 1',
                'rules' => 'required|trim'
    ],
    [
                'field' => 'address-line-2',
                'label' => 'Address Line 2',
                'rules' => 'required|trim'
    ],
    [
                'field' => 'postcode',
                'label' => 'Postcode',
                'rules' => 'required|numeric|trim'
    ],
    [
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'required|trim'
    ]

 );